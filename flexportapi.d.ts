/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * flexportpai.js
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
import { GetPurchaseOrderOptions, GetPurchaseOrdersOptions, UpsertPurchaseOrdersOptions } from './Interfaces/PurchaseOrder';
import { GetCompaniesOptions, GetCompanyOptions, GetMyCompanyOptions } from './Interfaces/Company';
import { GetLocationsOptions } from './Interfaces/Location';
export declare class FlexportAPI {
    private static CACHENAME;
    private readonly enableLogging;
    private readonly clientId;
    private readonly clientSecret;
    private readonly domain;
    constructor({ clientId, clientSecret, domain, enableLogging }: ConstructorOptions);
    private static get AccessTokenCache();
    /** @internal */
    private get AccessToken();
    clearAccessToken(): void;
    getMyCompany(options: GetMyCompanyOptions): void;
    getCompany(options: GetCompanyOptions): void;
    checkConnection(options: GetCompaniesOptions): void;
    getCompanies(options: GetCompaniesOptions): void;
    getLocations(options: GetLocationsOptions): void;
    getPurchaseOrder(options: GetPurchaseOrderOptions): void;
    getPurchaseOrders(options: GetPurchaseOrdersOptions): void;
    upsertPurchaseOrder(options: UpsertPurchaseOrdersOptions): void;
    private log;
    /** @internal */
    private makeGETRequest;
    private makePOSTRequest;
    /** @internal */
    private makeRequest;
}
export interface ConstructorOptions {
    clientId: string;
    clientSecret: string;
    domain: string;
    enableLogging?: boolean;
}
