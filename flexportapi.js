/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * flexportpai.js
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define(["require", "exports", "N/https", "N/error", "N/cache", "N/log"], function (require, exports, https, error, cache, log) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.FlexportAPI = void 0;
    // noinspection JSUnusedGlobalSymbols
    class FlexportAPI {
        constructor({ clientId, clientSecret, domain, enableLogging = false }) {
            this.domain = domain;
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.enableLogging = enableLogging;
        }
        static get AccessTokenCache() {
            return cache.getCache({ name: FlexportAPI.CACHENAME, scope: cache.Scope.PROTECTED });
        }
        /** @internal */
        get AccessToken() {
            let accessToken = FlexportAPI.AccessTokenCache.get({ key: this.clientId });
            if (!accessToken || !(accessToken.length > 5)) {
                accessToken = undefined;
                const accessTokenRequestOptions = {
                    method: https.Method.POST,
                    url: `https://${this.domain}/oauth/token`,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        client_id: this.clientId,
                        client_secret: this.clientSecret,
                        grant_type: 'client_credentials',
                        audience: 'https://api.flexport.com',
                    }),
                };
                this.log('audit', 'AccessToken - accessTokenRequestOptions', accessTokenRequestOptions);
                const clientResponse = https.request(accessTokenRequestOptions);
                this.log('audit', 'AccessToken - clientResponse', clientResponse);
                switch (+(clientResponse === null || clientResponse === void 0 ? void 0 : clientResponse.code)) {
                    case 200: {
                        const accessTokenResponse = JSON.parse(clientResponse.body);
                        accessToken = accessTokenResponse.access_token;
                        this.log('audit', 'AccessToken - caching', 'Caching AccessToken');
                        FlexportAPI.AccessTokenCache.put({
                            key: this.clientId,
                            value: accessToken,
                            ttl: +accessTokenResponse.expires_in - 100, // Lets expiry early, in case it took us time to get to his piece of code.
                        });
                        this.log('audit', 'AccessToken - caching', 'Access Token retrieval complete');
                        break;
                    }
                    default: {
                        throw error.create({ message: JSON.stringify(clientResponse), name: `BAD_CREDENTIALS` });
                    }
                }
                if (!accessToken) {
                    this.clearAccessToken();
                }
            }
            return accessToken;
        }
        clearAccessToken() {
            FlexportAPI.AccessTokenCache.remove({ key: this.clientId });
        }
        getMyCompany(options) {
            options.resource = 'network/me/companies';
            options.isSingle = true;
            this.makeGETRequest(options);
        }
        getCompany(options) {
            if (options.id) {
                options.isSingle = true;
                options.resource = `network/companies/${options.id}`;
                let queryString;
                if (options.expand) {
                    queryString = `?expand=${options.expand}`;
                }
                if (queryString) {
                    options.resource += queryString;
                }
                this.makeGETRequest(options);
            }
            else {
                throw error.create({ name: 'FlexportAPI - getCompany', message: 'id is required' });
            }
        }
        checkConnection(options) {
            options.resource = 'network/companies';
            // Let's ensure we only make one request
            options.isSingle = true;
            let queryString;
            if (options.expand) {
                queryString = `?expand=${options.expand}`;
            }
            if (options.filters) {
                for (const [filter, filterValue] of Object.entries(options.filters)) {
                    if (!queryString) {
                        queryString = '?';
                    }
                    else {
                        queryString += '&';
                    }
                    queryString += `${filter}=${filterValue}`;
                }
            }
            if (queryString) {
                options.resource += queryString;
            }
            this.makeGETRequest(options);
        }
        getCompanies(options) {
            options.resource = 'network/companies';
            let queryString;
            if (options.expand) {
                queryString = `?expand=${options.expand}`;
            }
            if (options.filters) {
                for (const [filter, filterValue] of Object.entries(options.filters)) {
                    if (!queryString) {
                        queryString = '?';
                    }
                    else {
                        queryString += '&';
                    }
                    queryString += `${filter}=${filterValue}`;
                }
            }
            if (queryString) {
                options.resource += queryString;
            }
            this.makeGETRequest(options);
        }
        getLocations(options) {
            options.resource = 'network/locations';
            let queryString;
            if (options.expand) {
                queryString = `?expand=${options.expand}`;
            }
            if (options.filters) {
                for (const [filter, filterValue] of Object.entries(options.filters)) {
                    if (!queryString) {
                        queryString = '?';
                    }
                    else {
                        queryString += '&';
                    }
                    queryString += `${filter}=${filterValue}`;
                }
            }
            if (queryString) {
                options.resource += queryString;
            }
            this.makeGETRequest(options);
        }
        getPurchaseOrder(options) {
            if (options.id) {
                if (options.id > 0) {
                    options.isSingle = true;
                    options.resource = `purchase_orders/${options.id}`;
                    let queryString;
                    if (options.expand) {
                        queryString = `?expand=${options.expand}`;
                    }
                    if (queryString) {
                        options.resource += queryString;
                    }
                    this.makeGETRequest(options);
                }
                else {
                    throw error.create({ name: 'FlexportAPI - getPurchaseOrder', message: `invalid id: ${options.id}` });
                }
            }
            else {
                throw error.create({ name: 'FlexportAPI - getPurchaseOrder', message: 'id is required' });
            }
        }
        getPurchaseOrders(options) {
            options.resource = 'purchase_orders';
            let queryString;
            if (options.expand) {
                queryString = `?expand=${options.expand}`;
            }
            if (options.filters) {
                for (const [filter, filterValue] of Object.entries(options.filters)) {
                    if (!queryString) {
                        queryString = '?';
                    }
                    else {
                        queryString += '&';
                    }
                    queryString += `${filter}=${filterValue}`;
                }
            }
            if (queryString) {
                options.resource += queryString;
            }
            this.makeGETRequest(options);
        }
        upsertPurchaseOrder(options) {
            options.resource = 'purchase_orders';
            if (options.purchaseOrder) {
                if (options.purchaseOrder.id) {
                    options.resource += `/${options.purchaseOrder.id}`;
                }
                options.payload = options.purchaseOrder;
                this.makePOSTRequest(options);
            }
            else {
                throw error.create({ name: 'FlexportAPI - upsertPurchaseOrder', message: 'purchaseOrder is required' });
            }
        }
        log(level, title, details) {
            if (this.enableLogging) {
                log[level]({
                    title: `FlexportAPI: ${title}`,
                    details: details,
                });
            }
        }
        /** @internal */
        makeGETRequest(options) {
            options.method = https.Method.GET;
            options.headers = {
                Accept: 'application/json',
            };
            this.makeRequest(options);
        }
        makePOSTRequest(options) {
            options.method = https.Method.POST;
            options.headers = {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            };
            this.makeRequest(options);
        }
        /** @internal */
        makeRequest(options) {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
            let clientResponse;
            try {
                options.headers['Authorization'] = `Bearer ${this.AccessToken}`;
            }
            catch (error_) {
                if (error_.name === 'BAD_CREDENTIALS') {
                    options.Unauthorized ? options.Unauthorized({ _object: '/api/error', code: '401', message: 'access_denied', status: 401 }) : (_a = options.Failed) === null || _a === void 0 ? void 0 : _a.call(options, clientResponse);
                }
                else {
                    throw error_;
                }
            }
            options.headers['Flexport-Version'] = '3';
            options.headers['Flexport-Log'] = 'SuiteApp';
            const requestOptions = {
                method: options.method,
                url: options.next || `https://${this.domain}/${options.resource}`,
                headers: options.headers,
            };
            if (options.payload) {
                requestOptions.body = JSON.stringify(options.payload);
            }
            try {
                this.log('audit', `makeRequest - requestOptions`, requestOptions);
                clientResponse = https.request(requestOptions);
                this.log('audit', `makeRequest - clientResponse`, clientResponse);
                switch (+clientResponse.code) {
                    case 200: {
                        if (options.isSingle) {
                            options.OK(JSON.parse(clientResponse.body));
                        }
                        else {
                            const collection = JSON.parse(clientResponse.body);
                            if (!options.collectionAggregator) {
                                options.collectionAggregator = collection;
                            }
                            else {
                                options.collectionAggregator.data.data = [...options.collectionAggregator.data.data, ...collection.data.data];
                            }
                            if (collection.data && collection.data.next && collection.data.next.length > 0) {
                                options.next = collection.data.next;
                                this.makeRequest(options);
                            }
                            else {
                                options.collectionAggregator.data.next = collection.data.next;
                                options.OK(options.collectionAggregator);
                            }
                        }
                        break;
                    }
                    case 201: {
                        (_b = options.Created) === null || _b === void 0 ? void 0 : _b.call(options, clientResponse);
                        break;
                    }
                    case 202: {
                        (_c = options.Accepted) === null || _c === void 0 ? void 0 : _c.call(options, clientResponse);
                        break;
                    }
                    case 400: {
                        options.BadRequest ? options.BadRequest(JSON.parse(clientResponse.body)) : (_d = options.Failed) === null || _d === void 0 ? void 0 : _d.call(options, clientResponse);
                        break;
                    }
                    case 401: {
                        const error = JSON.parse(clientResponse.body);
                        options.Unauthorized ? options.Unauthorized(error) : (_e = options.Failed) === null || _e === void 0 ? void 0 : _e.call(options, clientResponse);
                        break;
                    }
                    case 403: {
                        options.Forbidden ? options.Forbidden(clientResponse) : (_f = options.Failed) === null || _f === void 0 ? void 0 : _f.call(options, clientResponse);
                        break;
                    }
                    case 404: {
                        options.NotFound ? options.NotFound(clientResponse) : (_g = options.Failed) === null || _g === void 0 ? void 0 : _g.call(options, clientResponse);
                        break;
                    }
                    case 500: {
                        options.ServerError ? options.ServerError(clientResponse) : (_h = options.Failed) === null || _h === void 0 ? void 0 : _h.call(options, clientResponse);
                        break;
                    }
                    default: {
                        (_j = options.Failed) === null || _j === void 0 ? void 0 : _j.call(options, clientResponse);
                        break;
                    }
                }
            }
            catch (error_) {
                options.retryCount = options.retryCount || 0;
                if (options.retryCount <= 5) {
                    options.retryCount = options.retryCount + 1;
                    this.makeRequest(options);
                }
                else {
                    this.log('error', 'makeRequest', error_);
                    (_k = options.Failed) === null || _k === void 0 ? void 0 : _k.call(options, clientResponse);
                }
            }
        }
    }
    exports.FlexportAPI = FlexportAPI;
    FlexportAPI.CACHENAME = 'Flexport_AccessToken';
});
