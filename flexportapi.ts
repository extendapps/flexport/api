/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * flexportpai.js
 * @NApiVersion 2.1
 * @NModuleScope Public
 */

import * as https from 'N/https';
import * as error from 'N/error';
import * as cache from 'N/cache';
import * as log from 'N/log';
import {GetPurchaseOrderOptions, GetPurchaseOrdersOptions, UpsertPurchaseOrdersOptions} from './Interfaces/PurchaseOrder';
import {FlexportCollectionResponse, FlexportError, FlexportSingleResponse} from './Interfaces/BaseObject';
import {BaseOptions} from './Interfaces/BaseOptions';
import {GetCompaniesOptions, GetCompanyOptions, GetMyCompanyOptions} from './Interfaces/Company';
import {Company, PurchaseOrder} from './Interfaces/Objects';
import {GetLocationsOptions} from './Interfaces/Location';

// noinspection JSUnusedGlobalSymbols
export class FlexportAPI {
    private static CACHENAME = 'Flexport_AccessToken';
    private readonly enableLogging: boolean;
    private readonly clientId: string;
    private readonly clientSecret: string;
    private readonly domain: string;

    constructor({clientId, clientSecret, domain, enableLogging = false}: ConstructorOptions) {
        this.domain = domain;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.enableLogging = enableLogging;
    }

    private static get AccessTokenCache(): cache.Cache {
        return cache.getCache({name: FlexportAPI.CACHENAME, scope: cache.Scope.PROTECTED});
    }

    /** @internal */
    private get AccessToken(): string {
        let accessToken: string = FlexportAPI.AccessTokenCache.get({key: this.clientId});

        if (!accessToken || !(accessToken.length > 5)) {
            accessToken = undefined;
            const accessTokenRequestOptions: https.RequestOptions = {
                method: https.Method.POST,
                url: `https://${this.domain}/oauth/token`,
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    client_id: this.clientId,
                    client_secret: this.clientSecret,
                    grant_type: 'client_credentials',
                    audience: 'https://api.flexport.com',
                }),
            };

            this.log('audit', 'AccessToken - accessTokenRequestOptions', accessTokenRequestOptions);
            const clientResponse: https.ClientResponse = https.request(accessTokenRequestOptions);
            this.log('audit', 'AccessToken - clientResponse', clientResponse);

            switch (+clientResponse?.code) {
                case 200: {
                    const accessTokenResponse: AccessTokenResponse = JSON.parse(clientResponse.body);
                    accessToken = accessTokenResponse.access_token;

                    this.log('audit', 'AccessToken - caching', 'Caching AccessToken');
                    FlexportAPI.AccessTokenCache.put({
                        key: this.clientId,
                        value: accessToken,
                        ttl: +accessTokenResponse.expires_in - 100, // Lets expiry early, in case it took us time to get to his piece of code.
                    });
                    this.log('audit', 'AccessToken - caching', 'Access Token retrieval complete');
                    break;
                }

                default: {
                    throw error.create({message: JSON.stringify(clientResponse), name: `BAD_CREDENTIALS`});
                }
            }

            if (!accessToken) {
                this.clearAccessToken();
            }
        }

        return accessToken;
    }

    public clearAccessToken() {
        FlexportAPI.AccessTokenCache.remove({key: this.clientId});
    }

    public getMyCompany(options: GetMyCompanyOptions) {
        (options as unknown as MakeGETRequestOptions).resource = 'network/me/companies';
        (options as unknown as MakeGETRequestOptions).isSingle = true;
        this.makeGETRequest<Company>(options as unknown as MakeGETRequestOptions);
    }

    public getCompany(options: GetCompanyOptions) {
        if (options.id) {
            (options as unknown as MakeGETRequestOptions).isSingle = true;
            (options as unknown as MakeGETRequestOptions).resource = `network/companies/${options.id}`;
            let queryString;

            if (options.expand) {
                queryString = `?expand=${options.expand}`;
            }

            if (queryString) {
                (options as unknown as MakeGETRequestOptions).resource += queryString;
            }
            this.makeGETRequest<Company>(options as unknown as MakeGETRequestOptions);
        } else {
            throw error.create({name: 'FlexportAPI - getCompany', message: 'id is required'});
        }
    }

    public checkConnection(options: GetCompaniesOptions) {
        (options as unknown as MakeGETRequestOptions).resource = 'network/companies';

        // Let's ensure we only make one request
        (options as unknown as MakeGETRequestOptions).isSingle = true;

        let queryString;

        if (options.expand) {
            queryString = `?expand=${options.expand}`;
        }

        if (options.filters) {
            for (const [filter, filterValue] of Object.entries(options.filters)) {
                if (!queryString) {
                    queryString = '?';
                } else {
                    queryString += '&';
                }
                queryString += `${filter}=${filterValue}`;
            }
        }

        if (queryString) {
            (options as unknown as MakeGETRequestOptions).resource += queryString;
        }

        this.makeGETRequest<Company>(options as unknown as MakeGETRequestOptions);
    }

    public getCompanies(options: GetCompaniesOptions) {
        (options as unknown as MakeGETRequestOptions).resource = 'network/companies';
        let queryString;

        if (options.expand) {
            queryString = `?expand=${options.expand}`;
        }

        if (options.filters) {
            for (const [filter, filterValue] of Object.entries(options.filters)) {
                if (!queryString) {
                    queryString = '?';
                } else {
                    queryString += '&';
                }
                queryString += `${filter}=${filterValue}`;
            }
        }

        if (queryString) {
            (options as unknown as MakeGETRequestOptions).resource += queryString;
        }

        this.makeGETRequest<Company>(options as unknown as MakeGETRequestOptions);
    }

    public getLocations(options: GetLocationsOptions) {
        (options as unknown as MakeGETRequestOptions).resource = 'network/locations';
        let queryString;

        if (options.expand) {
            queryString = `?expand=${options.expand}`;
        }

        if (options.filters) {
            for (const [filter, filterValue] of Object.entries(options.filters)) {
                if (!queryString) {
                    queryString = '?';
                } else {
                    queryString += '&';
                }
                queryString += `${filter}=${filterValue}`;
            }
        }

        if (queryString) {
            (options as unknown as MakeGETRequestOptions).resource += queryString;
        }

        this.makeGETRequest<Company>(options as unknown as MakeGETRequestOptions);
    }

    public getPurchaseOrder(options: GetPurchaseOrderOptions) {
        if (options.id) {
            if (options.id > 0) {
                (options as unknown as MakeGETRequestOptions).isSingle = true;
                (options as unknown as MakeGETRequestOptions).resource = `purchase_orders/${options.id}`;
                let queryString;

                if (options.expand) {
                    queryString = `?expand=${options.expand}`;
                }

                if (queryString) {
                    (options as unknown as MakeGETRequestOptions).resource += queryString;
                }

                this.makeGETRequest<PurchaseOrder>(options as unknown as MakeGETRequestOptions);
            } else {
                throw error.create({name: 'FlexportAPI - getPurchaseOrder', message: `invalid id: ${options.id}`});
            }
        } else {
            throw error.create({name: 'FlexportAPI - getPurchaseOrder', message: 'id is required'});
        }
    }

    public getPurchaseOrders(options: GetPurchaseOrdersOptions) {
        (options as unknown as MakeGETRequestOptions).resource = 'purchase_orders';
        let queryString;

        if (options.expand) {
            queryString = `?expand=${options.expand}`;
        }

        if (options.filters) {
            for (const [filter, filterValue] of Object.entries(options.filters)) {
                if (!queryString) {
                    queryString = '?';
                } else {
                    queryString += '&';
                }
                queryString += `${filter}=${filterValue}`;
            }
        }

        if (queryString) {
            (options as unknown as MakeGETRequestOptions).resource += queryString;
        }

        this.makeGETRequest<PurchaseOrder>(options as unknown as MakeGETRequestOptions);
    }

    public upsertPurchaseOrder(options: UpsertPurchaseOrdersOptions) {
        (options as unknown as MakePOSTRequestOptions).resource = 'purchase_orders';

        if (options.purchaseOrder) {
            if (options.purchaseOrder.id) {
                (options as unknown as MakePOSTRequestOptions).resource += `/${options.purchaseOrder.id}`;
            }
            (options as unknown as MakePOSTRequestOptions).payload = options.purchaseOrder;
            this.makePOSTRequest(options as unknown as MakePOSTRequestOptions);
        } else {
            throw error.create({name: 'FlexportAPI - upsertPurchaseOrder', message: 'purchaseOrder is required'});
        }
    }

    private log(level: 'audit' | 'error' | 'emergency' | 'debug', title: string, details: unknown): void {
        if (this.enableLogging) {
            log[level]({
                title: `FlexportAPI: ${title}`,
                details: details,
            });
        }
    }

    /** @internal */
    private makeGETRequest<ObjectType>(options: MakeGETRequestOptions) {
        (options as unknown as MakeRequestOptions<ObjectType>).method = https.Method.GET;
        (options as unknown as MakeRequestOptions<ObjectType>).headers = {
            Accept: 'application/json',
        };

        this.makeRequest(options as unknown as MakeRequestOptions<ObjectType>);
    }

    private makePOSTRequest<ObjectType>(options: MakePOSTRequestOptions) {
        (options as unknown as MakeRequestOptions<ObjectType>).method = https.Method.POST;
        (options as unknown as MakeRequestOptions<ObjectType>).headers = {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        };
        this.makeRequest(options as unknown as MakeRequestOptions<ObjectType>);
    }

    /** @internal */
    private makeRequest<ObjectType>(options: MakeRequestOptions<ObjectType>) {
        let clientResponse: https.ClientResponse;
        try {
            options.headers['Authorization'] = `Bearer ${this.AccessToken}`;
        } catch (error_) {
            if (error_.name === 'BAD_CREDENTIALS') {
                options.Unauthorized ? options.Unauthorized({_object: '/api/error', code: '401', message: 'access_denied', status: 401}) : options.Failed?.(clientResponse);
            } else {
                throw error_;
            }
        }
        options.headers['Flexport-Version'] = '3';
        options.headers['Flexport-Log'] = 'SuiteApp';
        const requestOptions: https.RequestOptions = {
            method: options.method,
            url: options.next || `https://${this.domain}/${options.resource}`,
            headers: options.headers,
        };

        if (options.payload) {
            requestOptions.body = JSON.stringify(options.payload);
        }

        try {
            this.log('audit', `makeRequest - requestOptions`, requestOptions);
            clientResponse = https.request(requestOptions);
            this.log('audit', `makeRequest - clientResponse`, clientResponse);

            switch (+clientResponse.code) {
                case 200: {
                    if (options.isSingle) {
                        options.OK(<FlexportSingleResponse<ObjectType>>JSON.parse(clientResponse.body));
                    } else {
                        const collection = <FlexportCollectionResponse<ObjectType>>JSON.parse(clientResponse.body);

                        if (!options.collectionAggregator) {
                            options.collectionAggregator = collection;
                        } else {
                            options.collectionAggregator.data.data = [...options.collectionAggregator.data.data, ...collection.data.data];
                        }

                        if (collection.data && collection.data.next && collection.data.next.length > 0) {
                            options.next = collection.data.next;
                            this.makeRequest(options);
                        } else {
                            options.collectionAggregator.data.next = collection.data.next;
                            options.OK(options.collectionAggregator);
                        }
                    }

                    break;
                }

                case 201: {
                    options.Created?.(clientResponse);
                    break;
                }

                case 202: {
                    options.Accepted?.(clientResponse);
                    break;
                }

                case 400: {
                    options.BadRequest ? options.BadRequest(JSON.parse(clientResponse.body)) : options.Failed?.(clientResponse);
                    break;
                }

                case 401: {
                    const error: FlexportError = JSON.parse(clientResponse.body);
                    options.Unauthorized ? options.Unauthorized(error) : options.Failed?.(clientResponse);
                    break;
                }

                case 403: {
                    options.Forbidden ? options.Forbidden(clientResponse) : options.Failed?.(clientResponse);
                    break;
                }

                case 404: {
                    options.NotFound ? options.NotFound(clientResponse) : options.Failed?.(clientResponse);
                    break;
                }

                case 500: {
                    options.ServerError ? options.ServerError(clientResponse) : options.Failed?.(clientResponse);
                    break;
                }

                default: {
                    options.Failed?.(clientResponse);
                    break;
                }
            }
        } catch (error_) {
            options.retryCount = options.retryCount || 0;
            if (options.retryCount <= 5) {
                options.retryCount = options.retryCount + 1;
                this.makeRequest(options);
            } else {
                this.log('error', 'makeRequest', error_);
                options.Failed?.(clientResponse);
            }
        }
    }
}

/** @internal */
interface BaseInternalRequestOptions extends BaseOptions {
    resource: string;
    isSingle?: boolean;
}

/** @internal */
interface MakeGETRequestOptions extends BaseInternalRequestOptions {
    OK(clientResponse: https.ClientResponse): void;
}

/** @internal */
interface MakePOSTRequestOptions extends BaseInternalRequestOptions {
    payload: unknown;

    OK(clientResponse: https.ClientResponse): void;
}

/** @internal */
interface MakeRequestOptions<ObjectType> extends BaseInternalRequestOptions {
    method: https.Method;
    payload?: unknown;
    headers: unknown;
    retryCount?: number;
    next?: string;

    collectionAggregator?: FlexportCollectionResponse<ObjectType>;

    OK?(clientResponse: FlexportCollectionResponse<ObjectType> | FlexportSingleResponse<ObjectType>): void;

    Created?(clientResponse: https.ClientResponse): void;

    Accepted?(clientResponse: https.ClientResponse): void;

    Forbidden?(clientResponse: https.ClientResponse): void;

    NotFound?(clientResponse: https.ClientResponse): void;

    ServerError?(clientResponse: https.ClientResponse): void;
}

export interface ConstructorOptions {
    clientId: string;
    clientSecret: string;
    domain: string;
    enableLogging?: boolean;
}

/** @internal */
interface AccessTokenResponse {
    access_token: string;
    expires_in: number; // In seconds
    token_type: 'Bearer';
}
