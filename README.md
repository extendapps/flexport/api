# Flexport API NetSuite module

A distributable module for `Flexport` connectivity using NetSuite API's

See https://developers.flexport.com/s/api for more details

## Installation Instructions

`npm install --save-dev @extendapps/flexportapi`

Once installed, you'll need to make this module **relative** to you code in order to confirm to NetSuite's **relative** imports

### Dev-Ops Suggestions

Add the following commands to you "scripts" section in your package.json
> "flexportapi-setup": "cd Source; ln -s ../node_modules/\\@extendapps/flexportapi/"

> "flexportapi-deploy": "cp Source/flexportapi/flexportapi.js FileCabinet/PATH_TO_ROOT/",

### Update .gitignore

Add the following to you .gitignore file

> /Source/flexportapi/

### Create a symbolic link (Manual)

* Navigate to the root folder of you TypeScript source
* `ln -s ../node_modules/\@extendapps/flexportapi/`

This will create a **reference** to the flexportapi folder and allow you to use the appropriate import statement

`import {FlexportAPI} from './flexportapi';`

## Usage

### More Details

[Techincal Details](docs/README.md)
