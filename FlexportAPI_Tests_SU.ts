/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType Suitelet
 */

import {EntryPoints} from 'N/types';
import {FlexportAPI} from './flexportapi';
import * as https from 'N/https';
import * as runtime from 'N/runtime';
import * as log from 'N/log';
import {GetPurchaseOrderOptions, GetPurchaseOrdersOptions, UpsertPurchaseOrdersOptions} from './Interfaces/PurchaseOrder';
import {Company, PurchaseOrder} from './Interfaces/Objects';
import {GetCompaniesOptions, GetCompanyOptions} from './Interfaces/Company';
import {FlexportSingleResponse} from './Interfaces/BaseObject';
import {GetLocationsOptions} from './Interfaces/Location';

const getFilters = (context: EntryPoints.Suitelet.onRequestContext, getPurchaseOrdersOptions: GetPurchaseOrdersOptions) => {
    return JSON.parse(
        JSON.stringify(context.request.parameters, (key, value) => {
            if (key === 'expand') {
                getPurchaseOrdersOptions.expand = value;
            }
            return key === 'expand' || key === 'script' || key === 'deploy' || key === 'compid' || key === 'whence' ? undefined : value;
        })
    );
};

export const onRequest: EntryPoints.Suitelet.onRequest = (context: EntryPoints.Suitelet.onRequestContext) => {
    switch (context.request.method) {
        case https.Method.GET:
            // log.audit('context.request.parameters', context.request.parameters);

            const flexportAPI: FlexportAPI = new FlexportAPI({
                clientId: 'j4Mbt02K29PcD2V9RMDPKXnJOWZ9SFAR',
                clientSecret: 'iEt5jrhYs7FR2IOEyH4cZBNEome2rJnSQh5YjvUJdJMo8ukp0eHqn6Ex7_QP55M-',
                domain: 'api.sb.flexport.com',
                enableLogging: true,
            });

            flexportAPI.clearAccessToken();

            switch (runtime.getCurrentScript().deploymentId) {
                case 'customdeploy_flexport_get_po':
                    const getPurchaseOrderOptions: GetPurchaseOrderOptions = {
                        id: context.request.parameters.id ? +context.request.parameters.id : undefined,
                        OK: (flexportResponse: FlexportSingleResponse<PurchaseOrder>) => {
                            // log.audit('getPurchaseOrder - OK', purchaseOrder);
                            context.response.write(JSON.stringify(flexportResponse.data));
                        },
                        Failed: (clientResponse) => {
                            // log.error('getPurchaseOrder - Failed', clientResponse);
                            context.response.write(JSON.stringify(clientResponse));
                        },
                        Unauthorized: (error) => {
                            // log.error('getPurchaseOrder - Unauthorized', error);
                            context.response.write(JSON.stringify(error));
                        },
                    };

                    if (context.request.parameters.expand) {
                        getPurchaseOrderOptions.expand = context.request.parameters.expand;
                    }

                    log.audit('getPurchaseOrderOptions', getPurchaseOrderOptions);

                    flexportAPI.getPurchaseOrder(getPurchaseOrderOptions);
                    break;

                case 'customdeploy_flexport_get_pos':
                    const getPurchaseOrdersOptions: GetPurchaseOrdersOptions = {
                        OK: (purchaseOrders) => {
                            log.audit('getPurchaseOrders - OK', purchaseOrders);
                            context.response.write(JSON.stringify(purchaseOrders));
                        },
                        Failed: (clientResponse) => {
                            log.error('getPurchaseOrders - Failed', clientResponse);
                            context.response.write(JSON.stringify(clientResponse));
                        },
                        Unauthorized: (error) => {
                            log.error('getPurchaseOrders - Unauthorized', error);
                            context.response.write(JSON.stringify(error));
                        },
                    };

                    const filters = getFilters(context, getPurchaseOrdersOptions);

                    log.audit('filters', filters);
                    if (Object.keys(filters).length > 0) {
                        getPurchaseOrdersOptions.filters = filters;
                    }

                    if (context.request.parameters.expand) {
                        getPurchaseOrdersOptions.expand = context.request.parameters.expand;
                    }

                    log.audit('getPurchaseOrdersOptions', getPurchaseOrdersOptions);

                    flexportAPI.getPurchaseOrders(getPurchaseOrdersOptions);
                    break;

                case 'customdeploy_flexport_upsert_po':
                    const upsertPurchaseOrderOptions: UpsertPurchaseOrdersOptions = {
                        purchaseOrder: ValidPurchaseOrder,
                        OK: (purchaseOrder) => {
                            log.audit('upsertPurchaseOrder - OK', purchaseOrder);
                            context.response.write(JSON.stringify(purchaseOrder));
                        },
                        Failed: (clientResponse) => {
                            log.error('upsertPurchaseOrder - Failed', clientResponse);
                            context.response.write(JSON.stringify(clientResponse));
                        },
                        Unauthorized: (error) => {
                            log.error('upsertPurchaseOrder - Unauthorized', error);
                            context.response.write(JSON.stringify(error));
                        },
                        BadRequest: (error) => {
                            log.error('upsertPurchaseOrder - BadRequest', error);
                            context.response.write(JSON.stringify(error));
                        },
                    };

                    log.audit('upsertPurchaseOrderOptions', upsertPurchaseOrderOptions);

                    flexportAPI.upsertPurchaseOrder(upsertPurchaseOrderOptions);
                    break;

                case 'customdeploy_flexport_get_comp':
                    const getCompanyOptions: GetCompanyOptions = {
                        id: context.request.parameters.id,
                        OK: (flexportResponse: FlexportSingleResponse<Company>) => {
                            // log.audit('getCompany - OK', company);
                            context.response.write(JSON.stringify(flexportResponse));
                        },
                        Failed: (clientResponse) => {
                            // log.error('getCompany - Failed', clientResponse);
                            context.response.write(JSON.stringify(clientResponse));
                        },
                        Unauthorized: (error) => {
                            // log.error('getCompany - Unauthorized', error);
                            context.response.write(JSON.stringify(error));
                        },
                    };

                    if (context.request.parameters.expand) {
                        getCompanyOptions.expand = context.request.parameters.expand;
                    }

                    log.audit('getCompanyOptions', getCompanyOptions);

                    flexportAPI.getCompany(getCompanyOptions);
                    break;

                case 'customdeploy_flexport_get_comps':
                    const getCompaniesOptions: GetCompaniesOptions = {
                        OK: (companies) => {
                            // log.audit('getCompanies - OK', companies);
                            context.response.write(JSON.stringify(companies));
                        },
                        Failed: (clientResponse) => {
                            // log.error('getCompanies - Failed', clientResponse);
                            context.response.write(JSON.stringify(clientResponse));
                        },
                        Unauthorized: (error) => {
                            // log.error('getCompanies - Unauthorized', error);
                            context.response.write(JSON.stringify(error));
                        },
                    };

                    if (context.request.parameters.expand) {
                        getCompaniesOptions.expand = context.request.parameters.expand;
                    }

                    log.audit('getCompanyOpgetCompaniesOptionstions', getCompaniesOptions);

                    flexportAPI.getCompanies(getCompaniesOptions);
                    break;

                case 'customdeploy_flexport_get_locs':
                    const getLocationsOptions: GetLocationsOptions = {
                        filters: {
                            per: 100,
                        },
                        OK: (locations) => {
                            // log.audit('getLocationsOptions - OK', companies);
                            context.response.write(JSON.stringify(locations));
                        },
                        Failed: (clientResponse) => {
                            // log.error('getLocationsOptions - Failed', clientResponse);
                            context.response.write(JSON.stringify(clientResponse));
                        },
                        Unauthorized: (error) => {
                            // log.error('getLocationsOptions - Unauthorized', error);
                            context.response.write(JSON.stringify(error));
                        },
                    };

                    if (context.request.parameters.expand) {
                        getLocationsOptions.expand = context.request.parameters.expand;
                    }

                    log.audit('GetLocationsOptions', getLocationsOptions);

                    flexportAPI.getLocations(getLocationsOptions);
                    break;
            }
            break;
    }
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const ValidPurchaseOrder: PurchaseOrder = {
    status: 'open',
    issue_date: '2021-04-01T00:00:00',
    line_items: [
        {
            line_item_number: 1,
            must_arrive_date: '2021-12-01T00:00:00',
            transportation_mode: 'ocean',
            incoterm: 'FOB',
            product: {
                style: '',
                country_of_origin: '',
                ean_ucc_8: '',
                product_category: '',
                sku: '218238',
                lot_number: '',
                color: '',
                size: '',
                ean_ucc_13: '',
                dangerous: false,
                upc: '032231599145',
                name: 'Sylvania  Animal Asst 18x24 Asst-4',
            },
            metadata: {},
            item_key: '218238',
            units: 600,
            hs_codes: [
                {
                    country_code: 'US',
                    code: '4911.99.80.00',
                },
            ],
            unit_cost: {
                amount: '5.20',
                currency_code: 'USD',
            },
            measurements: [
                {
                    measure_type: 'length',
                    unit_of_measure: 'IN',
                    value: '20.906',
                },
                {
                    measure_type: 'width',
                    unit_of_measure: 'IN',
                    value: '26.969',
                },
                {
                    measure_type: 'height',
                    unit_of_measure: 'IN',
                    value: '10.039',
                },
                {
                    measure_type: 'net_weight',
                    unit_of_measure: 'LB',
                    value: '16.314',
                },
            ],
            cargo_ready_date: '2021-09-08T00:00:00',
        },
        {
            line_item_number: 2,
            must_arrive_date: '2021-12-01T00:00:00',
            transportation_mode: 'ocean',
            incoterm: 'FOB',
            product: {
                style: '',
                country_of_origin: '',
                ean_ucc_8: '',
                product_category: '',
                sku: '218233',
                lot_number: '',
                color: '',
                size: '',
                ean_ucc_13: '',
                dangerous: false,
                upc: '032231599091',
                name: 'Sylvania Tub Animal Asst 14x14 Asst-4',
            },
            metadata: {},
            item_key: '218233.1',
            units: 860,
            hs_codes: [
                {
                    country_code: 'US',
                    code: '4911.99.80.00',
                },
            ],
            unit_cost: {
                amount: '4.10',
                currency_code: 'USD',
            },
            measurements: [
                {
                    measure_type: 'length',
                    unit_of_measure: 'IN',
                    value: '16.929',
                },
                {
                    measure_type: 'width',
                    unit_of_measure: 'IN',
                    value: '16.535',
                },
                {
                    measure_type: 'height',
                    unit_of_measure: 'IN',
                    value: '10.039',
                },
                {
                    measure_type: 'net_weight',
                    unit_of_measure: 'LB',
                    value: '9.546',
                },
            ],
            cargo_ready_date: '2021-09-08T00:00:00',
        },
        {
            line_item_number: 3,
            must_arrive_date: '2021-12-01T00:00:00',
            transportation_mode: 'ocean',
            incoterm: 'FOB',
            product: {
                style: '',
                country_of_origin: '',
                ean_ucc_8: '',
                product_category: '',
                sku: '213965',
                lot_number: '',
                color: '',
                size: '',
                ean_ucc_13: '',
                dangerous: false,
                upc: '032231563092',
                name: 'Beatrice Chalk Calendar 15x24 Rustic-2',
            },
            metadata: {},
            item_key: '213965.2',
            units: 300,
            hs_codes: [
                {
                    country_code: 'US',
                    code: '4910.00.60.00',
                },
            ],
            unit_cost: {
                amount: '4.99',
                currency_code: 'USD',
            },
            measurements: [
                {
                    measure_type: 'length',
                    unit_of_measure: 'IN',
                    value: '20.118',
                },
                {
                    measure_type: 'width',
                    unit_of_measure: 'IN',
                    value: '4.134',
                },
                {
                    measure_type: 'height',
                    unit_of_measure: 'IN',
                    value: '29.134',
                },
                {
                    measure_type: 'net_weight',
                    unit_of_measure: 'LB',
                    value: '7.716',
                },
            ],
            cargo_ready_date: '2021-09-08T00:00:00',
        },
    ],
    memo: 'PN8128 Amazon Spring 2021 September  Commitments - Swades  (Dynamic)/Ref#21805, 21823, 21861,  21880, 21890, 21891, 21953, 21993,  22065, 22158/Pls accept order in FEDEX  within 48 hours.    11.6.20 MHU: Extended per pending PP  sample approval, targeting S/W  3/10-3/17/21.  1.5.21 MHU: Updated FOB of  221188/221202.  2.26.21 MHU: Extended line 8-17 and  19-28 per factory request.  3.10.21 MHU: Updated FOB of 221195 and  221196.  3.17.21 MHU: Updated FOB of 221191 and  221192.  3.19.21 MHU: Updated FOB of 221191 and  221192.. Updating all line quantities',
    origin_port: {
        code_type: 'unlocode',
        code: 'CNFOC',
    },
    order_class: 'purchase_order',
    transportation_mode: 'ocean',
    parties: [
        {
            role: 'buyer',
            external_ref: 'Main_Address',
        },
        {
            role: 'seller',
            external_ref: 'app_core_supply_ltd',
        },
    ],
    incoterm: 'FOB',
    name: 'xxxdx2',
};
