/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import * as https from 'N/https';
import {FlexportError} from './BaseObject';

export interface BaseOptions {
    Failed(clientResponse: https.ClientResponse): void;

    Unauthorized?(error: FlexportError): void;

    BadRequest?(error: FlexportError): void;
}

export interface BaseExpandableOptions extends BaseOptions {
    expand?: 'line_items' | 'line_items.booking_line_items' | 'locations' | 'contacts' | 'locations,contacts' | 'company';
}
