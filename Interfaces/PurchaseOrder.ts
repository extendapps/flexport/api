/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {PurchaseOrder} from './Objects';
import {BaseExpandableOptions, BaseOptions} from './BaseOptions';
import {FlexportCollectionResponse, FlexportSingleResponse} from './BaseObject';

/** @category Method */
export interface PurchaseOrderExpandable extends BaseExpandableOptions {
    /**
     * Include PurchaseOrderLineItem in the `line_items` property
     */
    expand?: 'line_items' | 'line_items.booking_line_items';
}

/** @category Method */
export interface GetPurchaseOrdersOptions extends PurchaseOrderExpandable {
    filters?: {
        /**
         * Page number of the page to retrieve
         */
        page?: number;
        /**
         * Count of items in each page Should be between 1 and 100 (inclusive)
         */
        per?: number;
        /**
         * Sort results by the specified field (only id is supported at this time)
         */
        sort?: string;
        /**
         * Set sort order. Allows "asc"(ascending) or "desc" (descending)
         */
        direction?: string;
        /**
         * Filter out all archived purchase orders (f.archived_at.exists=false) or filter out all unarchived purchase orders (f.archived_at.exists=true)
         */
        'f.archived_at.exists'?: boolean;
        /**
         * Filters the list based on the PO status
         */
        'f.status'?: string;
        /**
         * Filters the list based on PO buyer
         */
        'f.buyer_ref'?: string;
        /**
         * Filters the list based on the PO buyer entity
         */
        'f.seller_ref'?: string;
        /**
         * Filters the list based on which role you take in the PO. You can state multiple roles with a comma.
         */
        'f.roles'?: string;
        /**
         * Filter the list based on whether a PO is on the shipment matching the provided ID
         */
        'f.shipment.id'?: string;
        /**
         * Filter the list based on PO name
         */
        'f.name'?: string;
    };

    OK: (flexportResponse: FlexportCollectionResponse<PurchaseOrder>) => void;
}

/** @category Method */
export interface GetPurchaseOrderOptions extends PurchaseOrderExpandable {
    id: number;
    OK: (flexportResponse: FlexportSingleResponse<PurchaseOrder>) => void;
}

/** @category Method */
export interface UpsertPurchaseOrdersOptions extends BaseOptions {
    purchaseOrder: PurchaseOrder;

    OK: (flexportResponse: FlexportSingleResponse<PurchaseOrder>) => void;
}
