/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface FlexportSingleResponse<ObjectType> {
    /**
     * String representing the object’s type. Always /api/response for this object.
     */
    _object: '/api/response';
    /**
     * The full path to the resource(s) requested
     */
    self: string;
    /**
     * The version of the request
     */
    version: number;
    /**
     * The resource data requested for a successful response. null on error.
     * In the case of multiple results, this will contain the Pagination data
     */
    data: ObjectType;
    /**
     * The error object indicating what went wrong
     */
    error?: FlexportError;
}

export interface FlexportCollectionResponse<ObjectType> {
    /**
     * String representing the object’s type. Always /api/response for this object.
     */
    _object: '/api/response';
    /**
     * The full path to the resource(s) requested
     */
    self: string;
    /**
     * The version of the request
     */
    version: number;
    /**
     * The resource data requested for a successful response. null on error.
     * In the case of multiple results, this will contain the Pagination data
     */
    data: Pagination<ObjectType>;
    /**
     * The error object indicating what went wrong
     */
    errors?: FlexportError[];
}

export interface Pagination<ObjectType> {
    /**
     * String representing the object’s type. Always /api/collections/paginated for this object.
     */
    _object: '/api/collections/paginated';
    /**
     * link to the previous page
     */
    prev?: string;
    /**
     *
     */
    next?: string;
    /**
     * link to the next page
     */
    total_count: number;
    /**
     * list of elements in the current page
     */
    data: ObjectType[];
}

export interface FlexportError {
    /**
     * String representing the object’s type. Always /api/error for this object.
     */
    _object: '/api/error';
    /**
     * The HTTP status code (client or server error) made available for consumption from the body of the response
     */
    status: number;
    /**
     * Specialized identifier for this type of error in our API
     */
    code: string;
    /**
     * Human readable error message
     */
    message: string;
}
