/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
export type UnitOfMeasure =
    | 'BBL'
    | 'CAR'
    | 'CGM'
    | 'CKG'
    | 'CM'
    | 'CM2'
    | 'CTN'
    | 'CYK'
    | 'DOZ'
    | 'DPC'
    | 'DPR'
    | 'DS'
    | 'FBM'
    | 'G'
    | 'GBQ'
    | 'GR'
    | 'GRL'
    | 'HUN'
    | 'IRG'
    | 'JWL'
    | 'K'
    | 'KG'
    | 'KM3'
    | 'KM'
    | 'KWH'
    | 'L'
    | 'LNM'
    | 'M'
    | 'M2'
    | 'M3'
    | 'MBQ'
    | 'NO'
    | 'OSG'
    | 'PCS'
    | 'PDG'
    | 'PFL'
    | 'PK'
    | 'PRS'
    | 'PTG'
    | 'PX'
    | 'RHG'
    | 'RUG'
    | 'T'
    | 'TDWB'
    | 'THS'
    | 'W'
    | 'X'
    | '20'
    | '21'
    | '2W'
    | '43'
    | 'AS'
    | 'BA'
    | 'BC'
    | 'BD'
    | 'BG'
    | 'BJ'
    | 'BK'
    | 'BN'
    | 'BO'
    | 'BU'
    | 'BX'
    | 'CA'
    | 'CB'
    | 'CC'
    | 'CF'
    | 'CI'
    | 'CN'
    | 'CP'
    | 'CQ'
    | 'CR'
    | 'CS'
    | 'CT'
    | 'CX'
    | 'CY'
    | 'DF'
    | 'DH'
    | 'DR'
    | 'DZ'
    | 'EA'
    | 'FT'
    | 'GA'
    | 'GRAM'
    | 'GS'
    | 'IN'
    | 'JO'
    | 'JR'
    | 'LB'
    | 'LT'
    | 'MP'
    | 'MR'
    | 'MT'
    | 'OZ'
    | 'PC'
    | 'PQ'
    | 'PR'
    | 'PS'
    | 'PT'
    | 'QT'
    | 'RA'
    | 'RL'
    | 'RM'
    | 'RO'
    | 'SC'
    | 'SF'
    | 'SH'
    | 'SI'
    | 'SJ'
    | 'SL'
    | 'SM'
    | 'SO'
    | 'SQ'
    | 'ST'
    | 'SV'
    | 'SX'
    | 'SY'
    | 'TE'
    | 'TN'
    | 'TO'
    | 'UN'
    | 'YD'
    | 'Z3';
export type ShipmentLeg = Company;
export type Role = 'buyer' | 'seller' | 'owner' | 'shipper' | 'consignee' | 'freight_forwarder' | 'notify_party' | 'customs_broker' | 'carrier' | 'manufacturer' | 'buyers_agent' | 'sellers_agent';
export type BookingStatus = 'draft' | 'archived' | 'submitted' | 'booked' | 'shipment';
export type MeasureType = 'length' | 'width' | 'height' | 'gross_weight' | 'net_weight' | 'net_net_weight' | 'gross_volume' | 'volume_weight';
export type LineType = 'main_line' | 'sub_line' | 'component_line';
export type Incoterm = 'EXW' | 'FCA' | 'FAS' | 'FOB' | 'CPT' | 'CFR' | 'CIF' | 'CIP' | 'DAT' | 'DAP' | 'DDP' | 'DPU';
export type TransportMode = 'ocean' | 'air' | 'truck';
export type OrderClass = 'purchase_order' | 'sales_order' | 'transfer_order' | 'delivery_order' | 'work_order';
export type Status = 'open' | 'closed' | 'cancelled';
export type Priority = 'standard' | 'high';
export type FreightPaymentTerms = 'freight_collect' | 'freight_prepaid';
export type Tag = 'origin_address' | 'destination_address' | 'port_of_loading' | 'port_of_unloading' | 'port_of_calling' | 'fmc_port_of_unloading' | 'customs_entry' | 'transshipment' | 'place_of_delivery';

/* eslint-disable unicorn/prevent-abbreviations */
export interface Metadata {
    size?: string[];
}

export interface Address {
    _object: '/address';
    street_address: string;
    street_address2?: string;
    city: string;
    state: string;
    country: string;
    country_code: string;
    zip: string;
    unlocode?: string;
    timezone: string;
    ref: string;
}

export interface Detail {
    _object: '/air/port' | '/ocean/port' | '/trucking/port' | '/ocean/railport';
    port_code?: string;
}

export interface RailPortDetail extends Detail {
    _object: '/ocean/railport';
}

export interface RoadPortDetail extends Detail {
    _object: '/trucking/port';
}

export interface RoadPortDetail extends Detail {
    _object: '/trucking/port';
}

export interface SeaPortDetail extends Detail {
    _object: '/ocean/port';
}

export interface AirPortDetail extends Detail {
    _object: '/air/port';
    iata_code?: string;
    icao_code?: string;
}

export interface Place {
    _object?: '/place';
    name?: string;
    address?: Address;
    details?: Detail[];
    code_type?: 'unlocode';
    code?: string;
}

export interface CollectionRef<ObjectType> {
    _object: '/api/refs/collection' | '/api/collections/paginated';
    prev?: string;
    next?: string;
    total_count: number;
    data: ObjectType[];
    ref_type: string;
    link: string;
}

export interface VatNumber {
    _object: '/company_entity/vat_number';
    country_code: string;
    number: string;
}

export interface CompanyEntity {
    _object: '/company_entity';
    id: number;
    name: string;
    mailing_address: Address;
    ref: string;
    vat_numbers: VatNumber[];
}

export interface Location {
    _object: '/network/location';
    id: string;
    name: string;
    address: Address;
    editable: boolean;
    contacts: CollectionRef<Contact>;
    company: ObjectRef | Company;
    ref: string;
    metadata: Metadata;
}

export interface Company {
    _object: '/network/company';
    id: string;
    name: string;
    ref: string;
    editable: boolean;
    entities: CompanyEntity[];
    locations: CollectionRef<Location>;
    contacts: CollectionRef<Contact>;
    metadata: Metadata;
}

export interface Contact {
    _object: '/network/contact';
    id: string;
    name: string;
    email: string;
    phone_number: string;
    locations: CollectionRef<Location>;
    company: ObjectRef | ShipmentLeg;
}

export interface Party {
    _object?: '/purchase_orders/party' | 'purchase_orders/party';
    role: Role;
    company_entity?: CompanyEntity;
    location_address?: Address;
    contacts?: Contact[];
    external_ref?: string;
}

export interface ObjectRef {
    _object?: '/api/refs/object';
    ref_type: string;
    link: string;
    id: number;
}

export interface PurchaseOrderProduct {
    _object?: '/purchase_orders/line_item_product';
    style?: string;
    country_of_origin?: string;
    ean_ucc_8?: string;
    product_category?: string;
    sku: string;
    lot_number?: string;
    color?: string;
    size?: string;
    ean_ucc_13?: string;
    dangerous?: boolean;
    upc?: string;
    name: string;
}

export interface PurchaseOrderLineItemDestinationAddress {
    _object?: '/purchase_orders/line_items/destination_address';
    address: Address;
    units: number;
}

export interface HsCode {
    _object?: '/hs_code';
    description?: string;
    code: string;
    country_code: string;
}

export interface Money {
    _object?: '/money';
    amount: string;
    currency_code: string;
}

// TODO: Implement the Shipment Object Interface
export interface Shipment {
    id: number;
    _object: '/shipment';
}

export interface Terminal {
    _object: '/shipment_node/terminal';
    name: string;
    address: Address;
}

// TODO: Implement the ShipmentNode Object Interface
export interface ShipmentNode {
    _object: '/shipment_node';
    tags: Tag[];
    place: Place;
    terminal?: Terminal;
}

// TODO: Implement the OceanBookingDetail Object Interface
export interface OceanBookingDetail {
    _object: '/ocean/booking';
}

// TODO: Implement the AirBookingDetail Object Interface
export interface AirBookingDetail {
    _object: '/air/booking';
}

// TODO: Implement the TruckingBookingDetail Object Interface
export interface TruckingBookingDetail {
    _object: '/trucking/booking';
}

// TODO: Implement the Cargo Object Interface
export interface Cargo {
    _object: '/cargo';
}

export interface Booking {
    _object: '/booking';
    id: number;
    name: string;
    shipment: Shipment;
    status: BookingStatus;
    shipper_entity: CompanyEntity;
    consignee_entity: CompanyEntity;
    notify_party: string;
    transportation_mode: TransportMode;
    ocean_booking: OceanBookingDetail;
    air_booking: AirBookingDetail;
    trucking_booking: TruckingBookingDetail;
    origin_address: Address;
    cargo_ready_date: string;
    destination_address: Address;
    delivery_date: string;
    wants_export_customs_service: boolean;
    wants_import_customs_service: boolean;
    cargo: Cargo;
    booking_line_items: ObjectRef | BookingLineItem;
    special_instructions: string;
    created_at: string;
}

export interface BookingLineItem {
    _object?: '/purchase_orders/booking_line_item';
    id: number;
    booking: ObjectRef | Booking;
    purchase_order_line_item: ObjectRef | PurchaseOrderLineItem;
    units: number;
}

export interface LineItemMeasurement {
    _object?: '/line_item_measurement';
    measure_type: MeasureType;
    unit_of_measure: string;
    value: string;
}

export interface PurchaseOrderLineItem {
    _object?: '/purchase_orders/line_item';
    id?: number;
    line_type?: LineType;
    purchase_order?: ObjectRef | PurchaseOrder;
    line_item_number?: number;
    item_key: string;
    product: PurchaseOrderProduct;
    units: number;
    incoterm?: Incoterm;
    transportation_mode: TransportMode;
    unit_of_measure?: UnitOfMeasure;
    cargo_ready_date: string;
    origin_location_ref?: string;
    destination_locations?: {location_ref: string; units?: number}[];
    must_arrive_date: string;
    origin_port?: Place;
    origin_address?: Address;
    destination_port?: Place;
    destination_addresses?: PurchaseOrderLineItemDestinationAddress;
    hs_codes?: HsCode[];
    unit_cost: Money;
    booking_line_items?: CollectionRef<BookingLineItem>;
    parent_line_item_key?: string;
    assigned_party?: CompanyEntity;
    metadata?: Metadata;
    measurements?: LineItemMeasurement[];
}

export interface PurchaseOrder {
    _object?: '/purchase_order';
    name: string;
    incoterm: Incoterm;
    parties: Party[];
    transportation_mode: TransportMode;
    order_class: OrderClass;
    origin_port?: Place;
    memo: string;
    line_items: CollectionRef<PurchaseOrderLineItem> | PurchaseOrderLineItem[];
    issue_date: string;
    status: Status;
    metadata?: Metadata;
    id?: number;
    cargo_ready_date?: string;
    must_arrive_date?: string;
    origin_address?: Address;
    destination_port?: Place;
    destination_addresses?: Address[];
    destination_location_refs?: string[];
    archived_at?: string;
    created_at?: string;
    updated_at?: string;
    priority?: Priority;
    order_type?: string;
    freight_payment_terms?: FreightPaymentTerms;
}

export interface FlexportEvent {
    _object: '/event';
    id: number;
    type: 'ping' | '/shipment#created';
    created_at?: string; // 2021-04-01T00:00:00
    updated_at?: string; // 2021-04-01T00:00:00
    version: 3;
    data: ShipmentCreatedEvent;
}

// TODO: Implement the Container interface
export interface Container {
    _object: '/ocean/shipment_container';
}

export interface ShipmentCreatedEvent {
    _object: '/shipment_event_data';
    resource: Shipment;
    shipment: Shipment;
    containers: Container[];
}
