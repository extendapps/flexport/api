/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Location} from './Objects';
import {BaseExpandableOptions} from './BaseOptions';
import {FlexportCollectionResponse} from './BaseObject';

/** @category Method */
export interface LocationExpandable extends BaseExpandableOptions {
    /**
     * Include Company in the `company` property
     */
    expand?: 'company';
}

/** @category Method */
export interface GetLocationsOptions extends LocationExpandable {
    filters?: {
        /**
         * Page number of the page to retrieve
         */
        page?: number;
        /**
         * Count of items in each page Should be between 1 and 100 (inclusive)
         */
        per?: number;
        /**
         * The ref for the Location
         */
        'f.ref'?: string;
    };

    OK: (flexportResponse: FlexportCollectionResponse<Location>) => void;
}
