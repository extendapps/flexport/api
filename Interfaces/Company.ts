/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Company} from './Objects';
import {BaseExpandableOptions, BaseOptions} from './BaseOptions';
import {FlexportCollectionResponse, FlexportSingleResponse} from './BaseObject';

/** @category Method */
export interface CompanyExpandable extends BaseExpandableOptions {
    /**
     * Include PurchaseOrderLineItem in the `line_items` property
     */
    expand?: 'locations' | 'contacts' | 'locations,contacts';
}

/** @category Method */
export interface GetCompaniesOptions extends CompanyExpandable {
    filters?: {
        /**
         * Page number of the page to retrieve
         */
        page?: number;
        /**
         * Count of items in each page Should be between 1 and 100 (inclusive)
         */
        per?: number;
        /**
         * The ref for the company
         */
        'f.ref'?: string;
    };

    OK: (flexportResponse: FlexportCollectionResponse<Company>) => void;
}

/** @category Method */
export interface GetCompanyOptions extends CompanyExpandable {
    id: string;
    OK: (flexportResponse: FlexportSingleResponse<Company>) => void;
}

/** @category Method */
export interface GetMyCompanyOptions extends BaseOptions {
    OK: (flexportResponse: FlexportSingleResponse<Company>) => void;
}
