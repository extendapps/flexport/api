[Flexport API NetSuite module](../README.md) / [flexportapi](../modules/flexportapi.md) / FlexportAPI

# Class: FlexportAPI

[flexportapi](../modules/flexportapi.md).FlexportAPI

## Table of contents

### Methods

- [checkConnection](flexportapi.FlexportAPI.md#checkconnection)
- [clearAccessToken](flexportapi.FlexportAPI.md#clearaccesstoken)
- [getCompanies](flexportapi.FlexportAPI.md#getcompanies)
- [getCompany](flexportapi.FlexportAPI.md#getcompany)
- [getLocations](flexportapi.FlexportAPI.md#getlocations)
- [getMyCompany](flexportapi.FlexportAPI.md#getmycompany)
- [getPurchaseOrder](flexportapi.FlexportAPI.md#getpurchaseorder)
- [getPurchaseOrders](flexportapi.FlexportAPI.md#getpurchaseorders)
- [upsertPurchaseOrder](flexportapi.FlexportAPI.md#upsertpurchaseorder)

### Constructors

- [constructor](flexportapi.FlexportAPI.md#constructor)

## Methods

### checkConnection

▸ **checkConnection**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetCompaniesOptions`](../interfaces/Interfaces_Company.GetCompaniesOptions.md) |

#### Returns

`void`

___

### clearAccessToken

▸ **clearAccessToken**(): `void`

#### Returns

`void`

___

### getCompanies

▸ **getCompanies**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetCompaniesOptions`](../interfaces/Interfaces_Company.GetCompaniesOptions.md) |

#### Returns

`void`

___

### getCompany

▸ **getCompany**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetCompanyOptions`](../interfaces/Interfaces_Company.GetCompanyOptions.md) |

#### Returns

`void`

___

### getLocations

▸ **getLocations**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetLocationsOptions`](../interfaces/Interfaces_Location.GetLocationsOptions.md) |

#### Returns

`void`

___

### getMyCompany

▸ **getMyCompany**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetMyCompanyOptions`](../interfaces/Interfaces_Company.GetMyCompanyOptions.md) |

#### Returns

`void`

___

### getPurchaseOrder

▸ **getPurchaseOrder**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetPurchaseOrderOptions`](../interfaces/Interfaces_PurchaseOrder.GetPurchaseOrderOptions.md) |

#### Returns

`void`

___

### getPurchaseOrders

▸ **getPurchaseOrders**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetPurchaseOrdersOptions`](../interfaces/Interfaces_PurchaseOrder.GetPurchaseOrdersOptions.md) |

#### Returns

`void`

___

### upsertPurchaseOrder

▸ **upsertPurchaseOrder**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`UpsertPurchaseOrdersOptions`](../interfaces/Interfaces_PurchaseOrder.UpsertPurchaseOrdersOptions.md) |

#### Returns

`void`

## Constructors

### constructor

• **new FlexportAPI**(`__namedParameters`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`ConstructorOptions`](../interfaces/flexportapi.ConstructorOptions.md) |
