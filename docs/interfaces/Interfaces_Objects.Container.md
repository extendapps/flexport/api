[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Container

# Interface: Container

[Interfaces/Objects](../modules/Interfaces_Objects.md).Container

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Container.md#_object)

## Properties

### \_object

• **\_object**: ``"/ocean/shipment_container"``
