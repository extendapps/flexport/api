[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Place

# Interface: Place

[Interfaces/Objects](../modules/Interfaces_Objects.md).Place

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Place.md#_object)
- [address](Interfaces_Objects.Place.md#address)
- [code](Interfaces_Objects.Place.md#code)
- [code\_type](Interfaces_Objects.Place.md#code_type)
- [details](Interfaces_Objects.Place.md#details)
- [name](Interfaces_Objects.Place.md#name)

## Properties

### \_object

• `Optional` **\_object**: ``"/place"``

___

### address

• `Optional` **address**: [`Address`](Interfaces_Objects.Address.md)

___

### code

• `Optional` **code**: `string`

___

### code\_type

• `Optional` **code\_type**: ``"unlocode"``

___

### details

• `Optional` **details**: [`Detail`](Interfaces_Objects.Detail.md)[]

___

### name

• `Optional` **name**: `string`
