[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / ObjectRef

# Interface: ObjectRef

[Interfaces/Objects](../modules/Interfaces_Objects.md).ObjectRef

## Table of contents

### Properties

- [\_object](Interfaces_Objects.ObjectRef.md#_object)
- [id](Interfaces_Objects.ObjectRef.md#id)
- [link](Interfaces_Objects.ObjectRef.md#link)
- [ref\_type](Interfaces_Objects.ObjectRef.md#ref_type)

## Properties

### \_object

• `Optional` **\_object**: ``"/api/refs/object"``

___

### id

• **id**: `number`

___

### link

• **link**: `string`

___

### ref\_type

• **ref\_type**: `string`
