[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Detail

# Interface: Detail

[Interfaces/Objects](../modules/Interfaces_Objects.md).Detail

## Hierarchy

- **`Detail`**

  ↳ [`RailPortDetail`](Interfaces_Objects.RailPortDetail.md)

  ↳ [`RoadPortDetail`](Interfaces_Objects.RoadPortDetail.md)

  ↳ [`SeaPortDetail`](Interfaces_Objects.SeaPortDetail.md)

  ↳ [`AirPortDetail`](Interfaces_Objects.AirPortDetail.md)

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Detail.md#_object)
- [port\_code](Interfaces_Objects.Detail.md#port_code)

## Properties

### \_object

• **\_object**: ``"/air/port"`` \| ``"/ocean/port"`` \| ``"/trucking/port"`` \| ``"/ocean/railport"``

___

### port\_code

• `Optional` **port\_code**: `string`
