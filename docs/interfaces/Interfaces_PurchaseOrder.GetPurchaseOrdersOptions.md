[Flexport API NetSuite module](../README.md) / [Interfaces/PurchaseOrder](../modules/Interfaces_PurchaseOrder.md) / GetPurchaseOrdersOptions

# Interface: GetPurchaseOrdersOptions

[Interfaces/PurchaseOrder](../modules/Interfaces_PurchaseOrder.md).GetPurchaseOrdersOptions

## Hierarchy

- [`PurchaseOrderExpandable`](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md)

  ↳ **`GetPurchaseOrdersOptions`**

## Table of contents

### Methods

- [BadRequest](Interfaces_PurchaseOrder.GetPurchaseOrdersOptions.md#badrequest)
- [Failed](Interfaces_PurchaseOrder.GetPurchaseOrdersOptions.md#failed)
- [Unauthorized](Interfaces_PurchaseOrder.GetPurchaseOrdersOptions.md#unauthorized)

### Properties

- [OK](Interfaces_PurchaseOrder.GetPurchaseOrdersOptions.md#ok)
- [expand](Interfaces_PurchaseOrder.GetPurchaseOrdersOptions.md#expand)
- [filters](Interfaces_PurchaseOrder.GetPurchaseOrdersOptions.md#filters)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[PurchaseOrderExpandable](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md).[BadRequest](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[PurchaseOrderExpandable](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md).[Failed](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[PurchaseOrderExpandable](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md).[Unauthorized](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#unauthorized)

## Properties

### OK

• **OK**: (`flexportResponse`: [`FlexportCollectionResponse`](Interfaces_BaseObject.FlexportCollectionResponse.md)<[`PurchaseOrder`](Interfaces_Objects.PurchaseOrder.md)\>) => `void`

#### Type declaration

▸ (`flexportResponse`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `flexportResponse` | [`FlexportCollectionResponse`](Interfaces_BaseObject.FlexportCollectionResponse.md)<[`PurchaseOrder`](Interfaces_Objects.PurchaseOrder.md)\> |

##### Returns

`void`

___

### expand

• `Optional` **expand**: ``"line_items"`` \| ``"line_items.booking_line_items"``

Include PurchaseOrderLineItem in the `line_items` property

#### Inherited from

[PurchaseOrderExpandable](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md).[expand](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#expand)

___

### filters

• `Optional` **filters**: `Object`

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `direction?` | `string` | Set sort order. Allows "asc"(ascending) or "desc" (descending) |
| `f.archived_at.exists?` | `boolean` | Filter out all archived purchase orders (f.archived_at.exists=false) or filter out all unarchived purchase orders (f.archived_at.exists=true) |
| `f.buyer_ref?` | `string` | Filters the list based on PO buyer |
| `f.name?` | `string` | Filter the list based on PO name |
| `f.roles?` | `string` | Filters the list based on which role you take in the PO. You can state multiple roles with a comma. |
| `f.seller_ref?` | `string` | Filters the list based on the PO buyer entity |
| `f.shipment.id?` | `string` | Filter the list based on whether a PO is on the shipment matching the provided ID |
| `f.status?` | `string` | Filters the list based on the PO status |
| `page?` | `number` | Page number of the page to retrieve |
| `per?` | `number` | Count of items in each page Should be between 1 and 100 (inclusive) |
| `sort?` | `string` | Sort results by the specified field (only id is supported at this time) |
