[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Party

# Interface: Party

[Interfaces/Objects](../modules/Interfaces_Objects.md).Party

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Party.md#_object)
- [company\_entity](Interfaces_Objects.Party.md#company_entity)
- [contacts](Interfaces_Objects.Party.md#contacts)
- [external\_ref](Interfaces_Objects.Party.md#external_ref)
- [location\_address](Interfaces_Objects.Party.md#location_address)
- [role](Interfaces_Objects.Party.md#role)

## Properties

### \_object

• `Optional` **\_object**: ``"/purchase_orders/party"`` \| ``"purchase_orders/party"``

___

### company\_entity

• `Optional` **company\_entity**: [`CompanyEntity`](Interfaces_Objects.CompanyEntity.md)

___

### contacts

• `Optional` **contacts**: [`Contact`](Interfaces_Objects.Contact.md)[]

___

### external\_ref

• `Optional` **external\_ref**: `string`

___

### location\_address

• `Optional` **location\_address**: [`Address`](Interfaces_Objects.Address.md)

___

### role

• **role**: [`Role`](../modules/Interfaces_Objects.md#role)
