[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Shipment

# Interface: Shipment

[Interfaces/Objects](../modules/Interfaces_Objects.md).Shipment

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Shipment.md#_object)
- [id](Interfaces_Objects.Shipment.md#id)

## Properties

### \_object

• **\_object**: ``"/shipment"``

___

### id

• **id**: `number`
