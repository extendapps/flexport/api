[Flexport API NetSuite module](../README.md) / [Interfaces/PurchaseOrder](../modules/Interfaces_PurchaseOrder.md) / UpsertPurchaseOrdersOptions

# Interface: UpsertPurchaseOrdersOptions

[Interfaces/PurchaseOrder](../modules/Interfaces_PurchaseOrder.md).UpsertPurchaseOrdersOptions

## Hierarchy

- [`BaseOptions`](Interfaces_BaseOptions.BaseOptions.md)

  ↳ **`UpsertPurchaseOrdersOptions`**

## Table of contents

### Methods

- [BadRequest](Interfaces_PurchaseOrder.UpsertPurchaseOrdersOptions.md#badrequest)
- [Failed](Interfaces_PurchaseOrder.UpsertPurchaseOrdersOptions.md#failed)
- [Unauthorized](Interfaces_PurchaseOrder.UpsertPurchaseOrdersOptions.md#unauthorized)

### Properties

- [OK](Interfaces_PurchaseOrder.UpsertPurchaseOrdersOptions.md#ok)
- [purchaseOrder](Interfaces_PurchaseOrder.UpsertPurchaseOrdersOptions.md#purchaseorder)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseOptions](Interfaces_BaseOptions.BaseOptions.md).[BadRequest](Interfaces_BaseOptions.BaseOptions.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[BaseOptions](Interfaces_BaseOptions.BaseOptions.md).[Failed](Interfaces_BaseOptions.BaseOptions.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseOptions](Interfaces_BaseOptions.BaseOptions.md).[Unauthorized](Interfaces_BaseOptions.BaseOptions.md#unauthorized)

## Properties

### OK

• **OK**: (`flexportResponse`: [`FlexportSingleResponse`](Interfaces_BaseObject.FlexportSingleResponse.md)<[`PurchaseOrder`](Interfaces_Objects.PurchaseOrder.md)\>) => `void`

#### Type declaration

▸ (`flexportResponse`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `flexportResponse` | [`FlexportSingleResponse`](Interfaces_BaseObject.FlexportSingleResponse.md)<[`PurchaseOrder`](Interfaces_Objects.PurchaseOrder.md)\> |

##### Returns

`void`

___

### purchaseOrder

• **purchaseOrder**: [`PurchaseOrder`](Interfaces_Objects.PurchaseOrder.md)
