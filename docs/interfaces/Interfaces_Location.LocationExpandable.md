[Flexport API NetSuite module](../README.md) / [Interfaces/Location](../modules/Interfaces_Location.md) / LocationExpandable

# Interface: LocationExpandable

[Interfaces/Location](../modules/Interfaces_Location.md).LocationExpandable

## Hierarchy

- [`BaseExpandableOptions`](Interfaces_BaseOptions.BaseExpandableOptions.md)

  ↳ **`LocationExpandable`**

  ↳↳ [`GetLocationsOptions`](Interfaces_Location.GetLocationsOptions.md)

## Table of contents

### Methods

- [BadRequest](Interfaces_Location.LocationExpandable.md#badrequest)
- [Failed](Interfaces_Location.LocationExpandable.md#failed)
- [Unauthorized](Interfaces_Location.LocationExpandable.md#unauthorized)

### Properties

- [expand](Interfaces_Location.LocationExpandable.md#expand)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[BadRequest](Interfaces_BaseOptions.BaseExpandableOptions.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[Failed](Interfaces_BaseOptions.BaseExpandableOptions.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[Unauthorized](Interfaces_BaseOptions.BaseExpandableOptions.md#unauthorized)

## Properties

### expand

• `Optional` **expand**: ``"company"``

Include Company in the `company` property

#### Overrides

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[expand](Interfaces_BaseOptions.BaseExpandableOptions.md#expand)
