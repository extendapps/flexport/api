[Flexport API NetSuite module](../README.md) / [Interfaces/Company](../modules/Interfaces_Company.md) / GetCompanyOptions

# Interface: GetCompanyOptions

[Interfaces/Company](../modules/Interfaces_Company.md).GetCompanyOptions

## Hierarchy

- [`CompanyExpandable`](Interfaces_Company.CompanyExpandable.md)

  ↳ **`GetCompanyOptions`**

## Table of contents

### Methods

- [BadRequest](Interfaces_Company.GetCompanyOptions.md#badrequest)
- [Failed](Interfaces_Company.GetCompanyOptions.md#failed)
- [Unauthorized](Interfaces_Company.GetCompanyOptions.md#unauthorized)

### Properties

- [OK](Interfaces_Company.GetCompanyOptions.md#ok)
- [expand](Interfaces_Company.GetCompanyOptions.md#expand)
- [id](Interfaces_Company.GetCompanyOptions.md#id)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[CompanyExpandable](Interfaces_Company.CompanyExpandable.md).[BadRequest](Interfaces_Company.CompanyExpandable.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[CompanyExpandable](Interfaces_Company.CompanyExpandable.md).[Failed](Interfaces_Company.CompanyExpandable.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[CompanyExpandable](Interfaces_Company.CompanyExpandable.md).[Unauthorized](Interfaces_Company.CompanyExpandable.md#unauthorized)

## Properties

### OK

• **OK**: (`flexportResponse`: [`FlexportSingleResponse`](Interfaces_BaseObject.FlexportSingleResponse.md)<[`Company`](Interfaces_Objects.Company.md)\>) => `void`

#### Type declaration

▸ (`flexportResponse`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `flexportResponse` | [`FlexportSingleResponse`](Interfaces_BaseObject.FlexportSingleResponse.md)<[`Company`](Interfaces_Objects.Company.md)\> |

##### Returns

`void`

___

### expand

• `Optional` **expand**: ``"locations"`` \| ``"contacts"`` \| ``"locations,contacts"``

Include PurchaseOrderLineItem in the `line_items` property

#### Inherited from

[CompanyExpandable](Interfaces_Company.CompanyExpandable.md).[expand](Interfaces_Company.CompanyExpandable.md#expand)

___

### id

• **id**: `string`
