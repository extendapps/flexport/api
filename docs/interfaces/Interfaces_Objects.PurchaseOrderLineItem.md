[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / PurchaseOrderLineItem

# Interface: PurchaseOrderLineItem

[Interfaces/Objects](../modules/Interfaces_Objects.md).PurchaseOrderLineItem

## Table of contents

### Properties

- [\_object](Interfaces_Objects.PurchaseOrderLineItem.md#_object)
- [assigned\_party](Interfaces_Objects.PurchaseOrderLineItem.md#assigned_party)
- [booking\_line\_items](Interfaces_Objects.PurchaseOrderLineItem.md#booking_line_items)
- [cargo\_ready\_date](Interfaces_Objects.PurchaseOrderLineItem.md#cargo_ready_date)
- [destination\_addresses](Interfaces_Objects.PurchaseOrderLineItem.md#destination_addresses)
- [destination\_locations](Interfaces_Objects.PurchaseOrderLineItem.md#destination_locations)
- [destination\_port](Interfaces_Objects.PurchaseOrderLineItem.md#destination_port)
- [hs\_codes](Interfaces_Objects.PurchaseOrderLineItem.md#hs_codes)
- [id](Interfaces_Objects.PurchaseOrderLineItem.md#id)
- [incoterm](Interfaces_Objects.PurchaseOrderLineItem.md#incoterm)
- [item\_key](Interfaces_Objects.PurchaseOrderLineItem.md#item_key)
- [line\_item\_number](Interfaces_Objects.PurchaseOrderLineItem.md#line_item_number)
- [line\_type](Interfaces_Objects.PurchaseOrderLineItem.md#line_type)
- [measurements](Interfaces_Objects.PurchaseOrderLineItem.md#measurements)
- [metadata](Interfaces_Objects.PurchaseOrderLineItem.md#metadata)
- [must\_arrive\_date](Interfaces_Objects.PurchaseOrderLineItem.md#must_arrive_date)
- [origin\_address](Interfaces_Objects.PurchaseOrderLineItem.md#origin_address)
- [origin\_location\_ref](Interfaces_Objects.PurchaseOrderLineItem.md#origin_location_ref)
- [origin\_port](Interfaces_Objects.PurchaseOrderLineItem.md#origin_port)
- [parent\_line\_item\_key](Interfaces_Objects.PurchaseOrderLineItem.md#parent_line_item_key)
- [product](Interfaces_Objects.PurchaseOrderLineItem.md#product)
- [purchase\_order](Interfaces_Objects.PurchaseOrderLineItem.md#purchase_order)
- [transportation\_mode](Interfaces_Objects.PurchaseOrderLineItem.md#transportation_mode)
- [unit\_cost](Interfaces_Objects.PurchaseOrderLineItem.md#unit_cost)
- [unit\_of\_measure](Interfaces_Objects.PurchaseOrderLineItem.md#unit_of_measure)
- [units](Interfaces_Objects.PurchaseOrderLineItem.md#units)

## Properties

### \_object

• `Optional` **\_object**: ``"/purchase_orders/line_item"``

___

### assigned\_party

• `Optional` **assigned\_party**: [`CompanyEntity`](Interfaces_Objects.CompanyEntity.md)

___

### booking\_line\_items

• `Optional` **booking\_line\_items**: [`CollectionRef`](Interfaces_Objects.CollectionRef.md)<[`BookingLineItem`](Interfaces_Objects.BookingLineItem.md)\>

___

### cargo\_ready\_date

• **cargo\_ready\_date**: `string`

___

### destination\_addresses

• `Optional` **destination\_addresses**: [`PurchaseOrderLineItemDestinationAddress`](Interfaces_Objects.PurchaseOrderLineItemDestinationAddress.md)

___

### destination\_locations

• `Optional` **destination\_locations**: { `location_ref`: `string` ; `units?`: `number`  }[]

___

### destination\_port

• `Optional` **destination\_port**: [`Place`](Interfaces_Objects.Place.md)

___

### hs\_codes

• `Optional` **hs\_codes**: [`HsCode`](Interfaces_Objects.HsCode.md)[]

___

### id

• `Optional` **id**: `number`

___

### incoterm

• `Optional` **incoterm**: [`Incoterm`](../modules/Interfaces_Objects.md#incoterm)

___

### item\_key

• **item\_key**: `string`

___

### line\_item\_number

• `Optional` **line\_item\_number**: `number`

___

### line\_type

• `Optional` **line\_type**: [`LineType`](../modules/Interfaces_Objects.md#linetype)

___

### measurements

• `Optional` **measurements**: [`LineItemMeasurement`](Interfaces_Objects.LineItemMeasurement.md)[]

___

### metadata

• `Optional` **metadata**: [`Metadata`](Interfaces_Objects.Metadata.md)

___

### must\_arrive\_date

• **must\_arrive\_date**: `string`

___

### origin\_address

• `Optional` **origin\_address**: [`Address`](Interfaces_Objects.Address.md)

___

### origin\_location\_ref

• `Optional` **origin\_location\_ref**: `string`

___

### origin\_port

• `Optional` **origin\_port**: [`Place`](Interfaces_Objects.Place.md)

___

### parent\_line\_item\_key

• `Optional` **parent\_line\_item\_key**: `string`

___

### product

• **product**: [`PurchaseOrderProduct`](Interfaces_Objects.PurchaseOrderProduct.md)

___

### purchase\_order

• `Optional` **purchase\_order**: [`ObjectRef`](Interfaces_Objects.ObjectRef.md) \| [`PurchaseOrder`](Interfaces_Objects.PurchaseOrder.md)

___

### transportation\_mode

• **transportation\_mode**: [`TransportMode`](../modules/Interfaces_Objects.md#transportmode)

___

### unit\_cost

• **unit\_cost**: [`Money`](Interfaces_Objects.Money.md)

___

### unit\_of\_measure

• `Optional` **unit\_of\_measure**: [`UnitOfMeasure`](../modules/Interfaces_Objects.md#unitofmeasure)

___

### units

• **units**: `number`
