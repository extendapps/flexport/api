[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / TruckingBookingDetail

# Interface: TruckingBookingDetail

[Interfaces/Objects](../modules/Interfaces_Objects.md).TruckingBookingDetail

## Table of contents

### Properties

- [\_object](Interfaces_Objects.TruckingBookingDetail.md#_object)

## Properties

### \_object

• **\_object**: ``"/trucking/booking"``
