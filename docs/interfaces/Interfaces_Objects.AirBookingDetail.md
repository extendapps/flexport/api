[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / AirBookingDetail

# Interface: AirBookingDetail

[Interfaces/Objects](../modules/Interfaces_Objects.md).AirBookingDetail

## Table of contents

### Properties

- [\_object](Interfaces_Objects.AirBookingDetail.md#_object)

## Properties

### \_object

• **\_object**: ``"/air/booking"``
