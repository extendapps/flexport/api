[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Contact

# Interface: Contact

[Interfaces/Objects](../modules/Interfaces_Objects.md).Contact

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Contact.md#_object)
- [company](Interfaces_Objects.Contact.md#company)
- [email](Interfaces_Objects.Contact.md#email)
- [id](Interfaces_Objects.Contact.md#id)
- [locations](Interfaces_Objects.Contact.md#locations)
- [name](Interfaces_Objects.Contact.md#name)
- [phone\_number](Interfaces_Objects.Contact.md#phone_number)

## Properties

### \_object

• **\_object**: ``"/network/contact"``

___

### company

• **company**: [`Company`](Interfaces_Objects.Company.md) \| [`ObjectRef`](Interfaces_Objects.ObjectRef.md)

___

### email

• **email**: `string`

___

### id

• **id**: `string`

___

### locations

• **locations**: [`CollectionRef`](Interfaces_Objects.CollectionRef.md)<[`Location`](Interfaces_Objects.Location.md)\>

___

### name

• **name**: `string`

___

### phone\_number

• **phone\_number**: `string`
