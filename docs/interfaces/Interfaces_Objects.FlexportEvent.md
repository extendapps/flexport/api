[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / FlexportEvent

# Interface: FlexportEvent

[Interfaces/Objects](../modules/Interfaces_Objects.md).FlexportEvent

## Table of contents

### Properties

- [\_object](Interfaces_Objects.FlexportEvent.md#_object)
- [created\_at](Interfaces_Objects.FlexportEvent.md#created_at)
- [data](Interfaces_Objects.FlexportEvent.md#data)
- [id](Interfaces_Objects.FlexportEvent.md#id)
- [type](Interfaces_Objects.FlexportEvent.md#type)
- [updated\_at](Interfaces_Objects.FlexportEvent.md#updated_at)
- [version](Interfaces_Objects.FlexportEvent.md#version)

## Properties

### \_object

• **\_object**: ``"/event"``

___

### created\_at

• `Optional` **created\_at**: `string`

___

### data

• **data**: [`ShipmentCreatedEvent`](Interfaces_Objects.ShipmentCreatedEvent.md)

___

### id

• **id**: `number`

___

### type

• **type**: ``"ping"`` \| ``"/shipment#created"``

___

### updated\_at

• `Optional` **updated\_at**: `string`

___

### version

• **version**: ``3``
