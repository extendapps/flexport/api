[Flexport API NetSuite module](../README.md) / [Interfaces/BaseObject](../modules/Interfaces_BaseObject.md) / FlexportSingleResponse

# Interface: FlexportSingleResponse<ObjectType\>

[Interfaces/BaseObject](../modules/Interfaces_BaseObject.md).FlexportSingleResponse

**`Copyright`**

2021 ExtendApps, Inc.

**`Author`**

Darren Hill darren@extendapps.com

## Type parameters

| Name |
| :------ |
| `ObjectType` |

## Table of contents

### Properties

- [\_object](Interfaces_BaseObject.FlexportSingleResponse.md#_object)
- [data](Interfaces_BaseObject.FlexportSingleResponse.md#data)
- [error](Interfaces_BaseObject.FlexportSingleResponse.md#error)
- [self](Interfaces_BaseObject.FlexportSingleResponse.md#self)
- [version](Interfaces_BaseObject.FlexportSingleResponse.md#version)

## Properties

### \_object

• **\_object**: ``"/api/response"``

String representing the object’s type. Always /api/response for this object.

___

### data

• **data**: `ObjectType`

The resource data requested for a successful response. null on error.
In the case of multiple results, this will contain the Pagination data

___

### error

• `Optional` **error**: [`FlexportError`](Interfaces_BaseObject.FlexportError.md)

The error object indicating what went wrong

___

### self

• **self**: `string`

The full path to the resource(s) requested

___

### version

• **version**: `number`

The version of the request
