[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / CompanyEntity

# Interface: CompanyEntity

[Interfaces/Objects](../modules/Interfaces_Objects.md).CompanyEntity

## Table of contents

### Properties

- [\_object](Interfaces_Objects.CompanyEntity.md#_object)
- [id](Interfaces_Objects.CompanyEntity.md#id)
- [mailing\_address](Interfaces_Objects.CompanyEntity.md#mailing_address)
- [name](Interfaces_Objects.CompanyEntity.md#name)
- [ref](Interfaces_Objects.CompanyEntity.md#ref)
- [vat\_numbers](Interfaces_Objects.CompanyEntity.md#vat_numbers)

## Properties

### \_object

• **\_object**: ``"/company_entity"``

___

### id

• **id**: `number`

___

### mailing\_address

• **mailing\_address**: [`Address`](Interfaces_Objects.Address.md)

___

### name

• **name**: `string`

___

### ref

• **ref**: `string`

___

### vat\_numbers

• **vat\_numbers**: [`VatNumber`](Interfaces_Objects.VatNumber.md)[]
