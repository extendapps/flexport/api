[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / PurchaseOrder

# Interface: PurchaseOrder

[Interfaces/Objects](../modules/Interfaces_Objects.md).PurchaseOrder

## Table of contents

### Properties

- [\_object](Interfaces_Objects.PurchaseOrder.md#_object)
- [archived\_at](Interfaces_Objects.PurchaseOrder.md#archived_at)
- [cargo\_ready\_date](Interfaces_Objects.PurchaseOrder.md#cargo_ready_date)
- [created\_at](Interfaces_Objects.PurchaseOrder.md#created_at)
- [destination\_addresses](Interfaces_Objects.PurchaseOrder.md#destination_addresses)
- [destination\_location\_refs](Interfaces_Objects.PurchaseOrder.md#destination_location_refs)
- [destination\_port](Interfaces_Objects.PurchaseOrder.md#destination_port)
- [freight\_payment\_terms](Interfaces_Objects.PurchaseOrder.md#freight_payment_terms)
- [id](Interfaces_Objects.PurchaseOrder.md#id)
- [incoterm](Interfaces_Objects.PurchaseOrder.md#incoterm)
- [issue\_date](Interfaces_Objects.PurchaseOrder.md#issue_date)
- [line\_items](Interfaces_Objects.PurchaseOrder.md#line_items)
- [memo](Interfaces_Objects.PurchaseOrder.md#memo)
- [metadata](Interfaces_Objects.PurchaseOrder.md#metadata)
- [must\_arrive\_date](Interfaces_Objects.PurchaseOrder.md#must_arrive_date)
- [name](Interfaces_Objects.PurchaseOrder.md#name)
- [order\_class](Interfaces_Objects.PurchaseOrder.md#order_class)
- [order\_type](Interfaces_Objects.PurchaseOrder.md#order_type)
- [origin\_address](Interfaces_Objects.PurchaseOrder.md#origin_address)
- [origin\_port](Interfaces_Objects.PurchaseOrder.md#origin_port)
- [parties](Interfaces_Objects.PurchaseOrder.md#parties)
- [priority](Interfaces_Objects.PurchaseOrder.md#priority)
- [status](Interfaces_Objects.PurchaseOrder.md#status)
- [transportation\_mode](Interfaces_Objects.PurchaseOrder.md#transportation_mode)
- [updated\_at](Interfaces_Objects.PurchaseOrder.md#updated_at)

## Properties

### \_object

• `Optional` **\_object**: ``"/purchase_order"``

___

### archived\_at

• `Optional` **archived\_at**: `string`

___

### cargo\_ready\_date

• `Optional` **cargo\_ready\_date**: `string`

___

### created\_at

• `Optional` **created\_at**: `string`

___

### destination\_addresses

• `Optional` **destination\_addresses**: [`Address`](Interfaces_Objects.Address.md)[]

___

### destination\_location\_refs

• `Optional` **destination\_location\_refs**: `string`[]

___

### destination\_port

• `Optional` **destination\_port**: [`Place`](Interfaces_Objects.Place.md)

___

### freight\_payment\_terms

• `Optional` **freight\_payment\_terms**: [`FreightPaymentTerms`](../modules/Interfaces_Objects.md#freightpaymentterms)

___

### id

• `Optional` **id**: `number`

___

### incoterm

• **incoterm**: [`Incoterm`](../modules/Interfaces_Objects.md#incoterm)

___

### issue\_date

• **issue\_date**: `string`

___

### line\_items

• **line\_items**: [`CollectionRef`](Interfaces_Objects.CollectionRef.md)<[`PurchaseOrderLineItem`](Interfaces_Objects.PurchaseOrderLineItem.md)\> \| [`PurchaseOrderLineItem`](Interfaces_Objects.PurchaseOrderLineItem.md)[]

___

### memo

• **memo**: `string`

___

### metadata

• `Optional` **metadata**: [`Metadata`](Interfaces_Objects.Metadata.md)

___

### must\_arrive\_date

• `Optional` **must\_arrive\_date**: `string`

___

### name

• **name**: `string`

___

### order\_class

• **order\_class**: [`OrderClass`](../modules/Interfaces_Objects.md#orderclass)

___

### order\_type

• `Optional` **order\_type**: `string`

___

### origin\_address

• `Optional` **origin\_address**: [`Address`](Interfaces_Objects.Address.md)

___

### origin\_port

• `Optional` **origin\_port**: [`Place`](Interfaces_Objects.Place.md)

___

### parties

• **parties**: [`Party`](Interfaces_Objects.Party.md)[]

___

### priority

• `Optional` **priority**: [`Priority`](../modules/Interfaces_Objects.md#priority)

___

### status

• **status**: [`Status`](../modules/Interfaces_Objects.md#status)

___

### transportation\_mode

• **transportation\_mode**: [`TransportMode`](../modules/Interfaces_Objects.md#transportmode)

___

### updated\_at

• `Optional` **updated\_at**: `string`
