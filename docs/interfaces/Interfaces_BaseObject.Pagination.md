[Flexport API NetSuite module](../README.md) / [Interfaces/BaseObject](../modules/Interfaces_BaseObject.md) / Pagination

# Interface: Pagination<ObjectType\>

[Interfaces/BaseObject](../modules/Interfaces_BaseObject.md).Pagination

## Type parameters

| Name |
| :------ |
| `ObjectType` |

## Table of contents

### Properties

- [\_object](Interfaces_BaseObject.Pagination.md#_object)
- [data](Interfaces_BaseObject.Pagination.md#data)
- [next](Interfaces_BaseObject.Pagination.md#next)
- [prev](Interfaces_BaseObject.Pagination.md#prev)
- [total\_count](Interfaces_BaseObject.Pagination.md#total_count)

## Properties

### \_object

• **\_object**: ``"/api/collections/paginated"``

String representing the object’s type. Always /api/collections/paginated for this object.

___

### data

• **data**: `ObjectType`[]

list of elements in the current page

___

### next

• `Optional` **next**: `string`

___

### prev

• `Optional` **prev**: `string`

link to the previous page

___

### total\_count

• **total\_count**: `number`

link to the next page
