[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / RoadPortDetail

# Interface: RoadPortDetail

[Interfaces/Objects](../modules/Interfaces_Objects.md).RoadPortDetail

## Hierarchy

- [`Detail`](Interfaces_Objects.Detail.md)

  ↳ **`RoadPortDetail`**

## Table of contents

### Properties

- [\_object](Interfaces_Objects.RoadPortDetail.md#_object)
- [port\_code](Interfaces_Objects.RoadPortDetail.md#port_code)

## Properties

### \_object

• **\_object**: ``"/trucking/port"``

#### Inherited from

[Detail](Interfaces_Objects.Detail.md).[_object](Interfaces_Objects.Detail.md#_object)

___

### port\_code

• `Optional` **port\_code**: `string`

#### Inherited from

[Detail](Interfaces_Objects.Detail.md).[port_code](Interfaces_Objects.Detail.md#port_code)
