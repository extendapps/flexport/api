[Flexport API NetSuite module](../README.md) / [Interfaces/BaseOptions](../modules/Interfaces_BaseOptions.md) / BaseExpandableOptions

# Interface: BaseExpandableOptions

[Interfaces/BaseOptions](../modules/Interfaces_BaseOptions.md).BaseExpandableOptions

## Hierarchy

- [`BaseOptions`](Interfaces_BaseOptions.BaseOptions.md)

  ↳ **`BaseExpandableOptions`**

  ↳↳ [`PurchaseOrderExpandable`](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md)

  ↳↳ [`CompanyExpandable`](Interfaces_Company.CompanyExpandable.md)

  ↳↳ [`LocationExpandable`](Interfaces_Location.LocationExpandable.md)

## Table of contents

### Methods

- [BadRequest](Interfaces_BaseOptions.BaseExpandableOptions.md#badrequest)
- [Failed](Interfaces_BaseOptions.BaseExpandableOptions.md#failed)
- [Unauthorized](Interfaces_BaseOptions.BaseExpandableOptions.md#unauthorized)

### Properties

- [expand](Interfaces_BaseOptions.BaseExpandableOptions.md#expand)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseOptions](Interfaces_BaseOptions.BaseOptions.md).[BadRequest](Interfaces_BaseOptions.BaseOptions.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[BaseOptions](Interfaces_BaseOptions.BaseOptions.md).[Failed](Interfaces_BaseOptions.BaseOptions.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseOptions](Interfaces_BaseOptions.BaseOptions.md).[Unauthorized](Interfaces_BaseOptions.BaseOptions.md#unauthorized)

## Properties

### expand

• `Optional` **expand**: ``"line_items"`` \| ``"line_items.booking_line_items"`` \| ``"locations"`` \| ``"contacts"`` \| ``"locations,contacts"`` \| ``"company"``
