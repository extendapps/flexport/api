[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / ShipmentCreatedEvent

# Interface: ShipmentCreatedEvent

[Interfaces/Objects](../modules/Interfaces_Objects.md).ShipmentCreatedEvent

## Table of contents

### Properties

- [\_object](Interfaces_Objects.ShipmentCreatedEvent.md#_object)
- [containers](Interfaces_Objects.ShipmentCreatedEvent.md#containers)
- [resource](Interfaces_Objects.ShipmentCreatedEvent.md#resource)
- [shipment](Interfaces_Objects.ShipmentCreatedEvent.md#shipment)

## Properties

### \_object

• **\_object**: ``"/shipment_event_data"``

___

### containers

• **containers**: [`Container`](Interfaces_Objects.Container.md)[]

___

### resource

• **resource**: [`Shipment`](Interfaces_Objects.Shipment.md)

___

### shipment

• **shipment**: [`Shipment`](Interfaces_Objects.Shipment.md)
