[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Booking

# Interface: Booking

[Interfaces/Objects](../modules/Interfaces_Objects.md).Booking

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Booking.md#_object)
- [air\_booking](Interfaces_Objects.Booking.md#air_booking)
- [booking\_line\_items](Interfaces_Objects.Booking.md#booking_line_items)
- [cargo](Interfaces_Objects.Booking.md#cargo)
- [cargo\_ready\_date](Interfaces_Objects.Booking.md#cargo_ready_date)
- [consignee\_entity](Interfaces_Objects.Booking.md#consignee_entity)
- [created\_at](Interfaces_Objects.Booking.md#created_at)
- [delivery\_date](Interfaces_Objects.Booking.md#delivery_date)
- [destination\_address](Interfaces_Objects.Booking.md#destination_address)
- [id](Interfaces_Objects.Booking.md#id)
- [name](Interfaces_Objects.Booking.md#name)
- [notify\_party](Interfaces_Objects.Booking.md#notify_party)
- [ocean\_booking](Interfaces_Objects.Booking.md#ocean_booking)
- [origin\_address](Interfaces_Objects.Booking.md#origin_address)
- [shipment](Interfaces_Objects.Booking.md#shipment)
- [shipper\_entity](Interfaces_Objects.Booking.md#shipper_entity)
- [special\_instructions](Interfaces_Objects.Booking.md#special_instructions)
- [status](Interfaces_Objects.Booking.md#status)
- [transportation\_mode](Interfaces_Objects.Booking.md#transportation_mode)
- [trucking\_booking](Interfaces_Objects.Booking.md#trucking_booking)
- [wants\_export\_customs\_service](Interfaces_Objects.Booking.md#wants_export_customs_service)
- [wants\_import\_customs\_service](Interfaces_Objects.Booking.md#wants_import_customs_service)

## Properties

### \_object

• **\_object**: ``"/booking"``

___

### air\_booking

• **air\_booking**: [`AirBookingDetail`](Interfaces_Objects.AirBookingDetail.md)

___

### booking\_line\_items

• **booking\_line\_items**: [`ObjectRef`](Interfaces_Objects.ObjectRef.md) \| [`BookingLineItem`](Interfaces_Objects.BookingLineItem.md)

___

### cargo

• **cargo**: [`Cargo`](Interfaces_Objects.Cargo.md)

___

### cargo\_ready\_date

• **cargo\_ready\_date**: `string`

___

### consignee\_entity

• **consignee\_entity**: [`CompanyEntity`](Interfaces_Objects.CompanyEntity.md)

___

### created\_at

• **created\_at**: `string`

___

### delivery\_date

• **delivery\_date**: `string`

___

### destination\_address

• **destination\_address**: [`Address`](Interfaces_Objects.Address.md)

___

### id

• **id**: `number`

___

### name

• **name**: `string`

___

### notify\_party

• **notify\_party**: `string`

___

### ocean\_booking

• **ocean\_booking**: [`OceanBookingDetail`](Interfaces_Objects.OceanBookingDetail.md)

___

### origin\_address

• **origin\_address**: [`Address`](Interfaces_Objects.Address.md)

___

### shipment

• **shipment**: [`Shipment`](Interfaces_Objects.Shipment.md)

___

### shipper\_entity

• **shipper\_entity**: [`CompanyEntity`](Interfaces_Objects.CompanyEntity.md)

___

### special\_instructions

• **special\_instructions**: `string`

___

### status

• **status**: [`BookingStatus`](../modules/Interfaces_Objects.md#bookingstatus)

___

### transportation\_mode

• **transportation\_mode**: [`TransportMode`](../modules/Interfaces_Objects.md#transportmode)

___

### trucking\_booking

• **trucking\_booking**: [`TruckingBookingDetail`](Interfaces_Objects.TruckingBookingDetail.md)

___

### wants\_export\_customs\_service

• **wants\_export\_customs\_service**: `boolean`

___

### wants\_import\_customs\_service

• **wants\_import\_customs\_service**: `boolean`
