[Flexport API NetSuite module](../README.md) / [Interfaces/PurchaseOrder](../modules/Interfaces_PurchaseOrder.md) / GetPurchaseOrderOptions

# Interface: GetPurchaseOrderOptions

[Interfaces/PurchaseOrder](../modules/Interfaces_PurchaseOrder.md).GetPurchaseOrderOptions

## Hierarchy

- [`PurchaseOrderExpandable`](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md)

  ↳ **`GetPurchaseOrderOptions`**

## Table of contents

### Methods

- [BadRequest](Interfaces_PurchaseOrder.GetPurchaseOrderOptions.md#badrequest)
- [Failed](Interfaces_PurchaseOrder.GetPurchaseOrderOptions.md#failed)
- [Unauthorized](Interfaces_PurchaseOrder.GetPurchaseOrderOptions.md#unauthorized)

### Properties

- [OK](Interfaces_PurchaseOrder.GetPurchaseOrderOptions.md#ok)
- [expand](Interfaces_PurchaseOrder.GetPurchaseOrderOptions.md#expand)
- [id](Interfaces_PurchaseOrder.GetPurchaseOrderOptions.md#id)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[PurchaseOrderExpandable](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md).[BadRequest](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[PurchaseOrderExpandable](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md).[Failed](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[PurchaseOrderExpandable](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md).[Unauthorized](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#unauthorized)

## Properties

### OK

• **OK**: (`flexportResponse`: [`FlexportSingleResponse`](Interfaces_BaseObject.FlexportSingleResponse.md)<[`PurchaseOrder`](Interfaces_Objects.PurchaseOrder.md)\>) => `void`

#### Type declaration

▸ (`flexportResponse`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `flexportResponse` | [`FlexportSingleResponse`](Interfaces_BaseObject.FlexportSingleResponse.md)<[`PurchaseOrder`](Interfaces_Objects.PurchaseOrder.md)\> |

##### Returns

`void`

___

### expand

• `Optional` **expand**: ``"line_items"`` \| ``"line_items.booking_line_items"``

Include PurchaseOrderLineItem in the `line_items` property

#### Inherited from

[PurchaseOrderExpandable](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md).[expand](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#expand)

___

### id

• **id**: `number`
