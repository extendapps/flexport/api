[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / ShipmentNode

# Interface: ShipmentNode

[Interfaces/Objects](../modules/Interfaces_Objects.md).ShipmentNode

## Table of contents

### Properties

- [\_object](Interfaces_Objects.ShipmentNode.md#_object)
- [place](Interfaces_Objects.ShipmentNode.md#place)
- [tags](Interfaces_Objects.ShipmentNode.md#tags)
- [terminal](Interfaces_Objects.ShipmentNode.md#terminal)

## Properties

### \_object

• **\_object**: ``"/shipment_node"``

___

### place

• **place**: [`Place`](Interfaces_Objects.Place.md)

___

### tags

• **tags**: [`Tag`](../modules/Interfaces_Objects.md#tag)[]

___

### terminal

• `Optional` **terminal**: [`Terminal`](Interfaces_Objects.Terminal.md)
