[Flexport API NetSuite module](../README.md) / [Interfaces/BaseOptions](../modules/Interfaces_BaseOptions.md) / BaseOptions

# Interface: BaseOptions

[Interfaces/BaseOptions](../modules/Interfaces_BaseOptions.md).BaseOptions

## Hierarchy

- **`BaseOptions`**

  ↳ [`BaseExpandableOptions`](Interfaces_BaseOptions.BaseExpandableOptions.md)

  ↳ [`UpsertPurchaseOrdersOptions`](Interfaces_PurchaseOrder.UpsertPurchaseOrdersOptions.md)

## Table of contents

### Methods

- [BadRequest](Interfaces_BaseOptions.BaseOptions.md#badrequest)
- [Failed](Interfaces_BaseOptions.BaseOptions.md#failed)
- [Unauthorized](Interfaces_BaseOptions.BaseOptions.md#unauthorized)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`
