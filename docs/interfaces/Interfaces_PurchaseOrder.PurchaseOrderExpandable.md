[Flexport API NetSuite module](../README.md) / [Interfaces/PurchaseOrder](../modules/Interfaces_PurchaseOrder.md) / PurchaseOrderExpandable

# Interface: PurchaseOrderExpandable

[Interfaces/PurchaseOrder](../modules/Interfaces_PurchaseOrder.md).PurchaseOrderExpandable

## Hierarchy

- [`BaseExpandableOptions`](Interfaces_BaseOptions.BaseExpandableOptions.md)

  ↳ **`PurchaseOrderExpandable`**

  ↳↳ [`GetPurchaseOrdersOptions`](Interfaces_PurchaseOrder.GetPurchaseOrdersOptions.md)

  ↳↳ [`GetPurchaseOrderOptions`](Interfaces_PurchaseOrder.GetPurchaseOrderOptions.md)

## Table of contents

### Methods

- [BadRequest](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#badrequest)
- [Failed](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#failed)
- [Unauthorized](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#unauthorized)

### Properties

- [expand](Interfaces_PurchaseOrder.PurchaseOrderExpandable.md#expand)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[BadRequest](Interfaces_BaseOptions.BaseExpandableOptions.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[Failed](Interfaces_BaseOptions.BaseExpandableOptions.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[Unauthorized](Interfaces_BaseOptions.BaseExpandableOptions.md#unauthorized)

## Properties

### expand

• `Optional` **expand**: ``"line_items"`` \| ``"line_items.booking_line_items"``

Include PurchaseOrderLineItem in the `line_items` property

#### Overrides

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[expand](Interfaces_BaseOptions.BaseExpandableOptions.md#expand)
