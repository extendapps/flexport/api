[Flexport API NetSuite module](../README.md) / [Interfaces/Company](../modules/Interfaces_Company.md) / GetMyCompanyOptions

# Interface: GetMyCompanyOptions

[Interfaces/Company](../modules/Interfaces_Company.md).GetMyCompanyOptions

## Table of contents

### Properties

- [OK](Interfaces_Company.GetMyCompanyOptions.md#ok)

## Properties

### OK

• **OK**: (`flexportResponse`: [`FlexportSingleResponse`](Interfaces_BaseObject.FlexportSingleResponse.md)<[`Company`](Interfaces_Objects.Company.md)\>) => `void`

#### Type declaration

▸ (`flexportResponse`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `flexportResponse` | [`FlexportSingleResponse`](Interfaces_BaseObject.FlexportSingleResponse.md)<[`Company`](Interfaces_Objects.Company.md)\> |

##### Returns

`void`
