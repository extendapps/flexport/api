[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / SeaPortDetail

# Interface: SeaPortDetail

[Interfaces/Objects](../modules/Interfaces_Objects.md).SeaPortDetail

## Hierarchy

- [`Detail`](Interfaces_Objects.Detail.md)

  ↳ **`SeaPortDetail`**

## Table of contents

### Properties

- [\_object](Interfaces_Objects.SeaPortDetail.md#_object)
- [port\_code](Interfaces_Objects.SeaPortDetail.md#port_code)

## Properties

### \_object

• **\_object**: ``"/ocean/port"``

#### Overrides

[Detail](Interfaces_Objects.Detail.md).[_object](Interfaces_Objects.Detail.md#_object)

___

### port\_code

• `Optional` **port\_code**: `string`

#### Inherited from

[Detail](Interfaces_Objects.Detail.md).[port_code](Interfaces_Objects.Detail.md#port_code)
