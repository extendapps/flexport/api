[Flexport API NetSuite module](../README.md) / [Interfaces/Company](../modules/Interfaces_Company.md) / CompanyExpandable

# Interface: CompanyExpandable

[Interfaces/Company](../modules/Interfaces_Company.md).CompanyExpandable

## Hierarchy

- [`BaseExpandableOptions`](Interfaces_BaseOptions.BaseExpandableOptions.md)

  ↳ **`CompanyExpandable`**

  ↳↳ [`GetCompaniesOptions`](Interfaces_Company.GetCompaniesOptions.md)

  ↳↳ [`GetCompanyOptions`](Interfaces_Company.GetCompanyOptions.md)

## Table of contents

### Methods

- [BadRequest](Interfaces_Company.CompanyExpandable.md#badrequest)
- [Failed](Interfaces_Company.CompanyExpandable.md#failed)
- [Unauthorized](Interfaces_Company.CompanyExpandable.md#unauthorized)

### Properties

- [expand](Interfaces_Company.CompanyExpandable.md#expand)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[BadRequest](Interfaces_BaseOptions.BaseExpandableOptions.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[Failed](Interfaces_BaseOptions.BaseExpandableOptions.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[Unauthorized](Interfaces_BaseOptions.BaseExpandableOptions.md#unauthorized)

## Properties

### expand

• `Optional` **expand**: ``"locations"`` \| ``"contacts"`` \| ``"locations,contacts"``

Include PurchaseOrderLineItem in the `line_items` property

#### Overrides

[BaseExpandableOptions](Interfaces_BaseOptions.BaseExpandableOptions.md).[expand](Interfaces_BaseOptions.BaseExpandableOptions.md#expand)
