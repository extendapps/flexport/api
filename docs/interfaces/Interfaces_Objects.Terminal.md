[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Terminal

# Interface: Terminal

[Interfaces/Objects](../modules/Interfaces_Objects.md).Terminal

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Terminal.md#_object)
- [address](Interfaces_Objects.Terminal.md#address)
- [name](Interfaces_Objects.Terminal.md#name)

## Properties

### \_object

• **\_object**: ``"/shipment_node/terminal"``

___

### address

• **address**: [`Address`](Interfaces_Objects.Address.md)

___

### name

• **name**: `string`
