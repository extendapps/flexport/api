[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Money

# Interface: Money

[Interfaces/Objects](../modules/Interfaces_Objects.md).Money

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Money.md#_object)
- [amount](Interfaces_Objects.Money.md#amount)
- [currency\_code](Interfaces_Objects.Money.md#currency_code)

## Properties

### \_object

• `Optional` **\_object**: ``"/money"``

___

### amount

• **amount**: `string`

___

### currency\_code

• **currency\_code**: `string`
