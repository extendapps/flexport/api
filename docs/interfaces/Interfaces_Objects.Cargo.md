[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Cargo

# Interface: Cargo

[Interfaces/Objects](../modules/Interfaces_Objects.md).Cargo

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Cargo.md#_object)

## Properties

### \_object

• **\_object**: ``"/cargo"``
