[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / RailPortDetail

# Interface: RailPortDetail

[Interfaces/Objects](../modules/Interfaces_Objects.md).RailPortDetail

## Hierarchy

- [`Detail`](Interfaces_Objects.Detail.md)

  ↳ **`RailPortDetail`**

## Table of contents

### Properties

- [\_object](Interfaces_Objects.RailPortDetail.md#_object)
- [port\_code](Interfaces_Objects.RailPortDetail.md#port_code)

## Properties

### \_object

• **\_object**: ``"/ocean/railport"``

#### Overrides

[Detail](Interfaces_Objects.Detail.md).[_object](Interfaces_Objects.Detail.md#_object)

___

### port\_code

• `Optional` **port\_code**: `string`

#### Inherited from

[Detail](Interfaces_Objects.Detail.md).[port_code](Interfaces_Objects.Detail.md#port_code)
