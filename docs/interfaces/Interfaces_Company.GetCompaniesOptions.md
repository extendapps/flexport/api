[Flexport API NetSuite module](../README.md) / [Interfaces/Company](../modules/Interfaces_Company.md) / GetCompaniesOptions

# Interface: GetCompaniesOptions

[Interfaces/Company](../modules/Interfaces_Company.md).GetCompaniesOptions

## Hierarchy

- [`CompanyExpandable`](Interfaces_Company.CompanyExpandable.md)

  ↳ **`GetCompaniesOptions`**

## Table of contents

### Methods

- [BadRequest](Interfaces_Company.GetCompaniesOptions.md#badrequest)
- [Failed](Interfaces_Company.GetCompaniesOptions.md#failed)
- [Unauthorized](Interfaces_Company.GetCompaniesOptions.md#unauthorized)

### Properties

- [OK](Interfaces_Company.GetCompaniesOptions.md#ok)
- [expand](Interfaces_Company.GetCompaniesOptions.md#expand)
- [filters](Interfaces_Company.GetCompaniesOptions.md#filters)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[CompanyExpandable](Interfaces_Company.CompanyExpandable.md).[BadRequest](Interfaces_Company.CompanyExpandable.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[CompanyExpandable](Interfaces_Company.CompanyExpandable.md).[Failed](Interfaces_Company.CompanyExpandable.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[CompanyExpandable](Interfaces_Company.CompanyExpandable.md).[Unauthorized](Interfaces_Company.CompanyExpandable.md#unauthorized)

## Properties

### OK

• **OK**: (`flexportResponse`: [`FlexportCollectionResponse`](Interfaces_BaseObject.FlexportCollectionResponse.md)<[`Company`](Interfaces_Objects.Company.md)\>) => `void`

#### Type declaration

▸ (`flexportResponse`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `flexportResponse` | [`FlexportCollectionResponse`](Interfaces_BaseObject.FlexportCollectionResponse.md)<[`Company`](Interfaces_Objects.Company.md)\> |

##### Returns

`void`

___

### expand

• `Optional` **expand**: ``"locations"`` \| ``"contacts"`` \| ``"locations,contacts"``

Include PurchaseOrderLineItem in the `line_items` property

#### Inherited from

[CompanyExpandable](Interfaces_Company.CompanyExpandable.md).[expand](Interfaces_Company.CompanyExpandable.md#expand)

___

### filters

• `Optional` **filters**: `Object`

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `f.ref?` | `string` | The ref for the company |
| `page?` | `number` | Page number of the page to retrieve |
| `per?` | `number` | Count of items in each page Should be between 1 and 100 (inclusive) |
