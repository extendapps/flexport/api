[Flexport API NetSuite module](../README.md) / [flexportapi](../modules/flexportapi.md) / ConstructorOptions

# Interface: ConstructorOptions

[flexportapi](../modules/flexportapi.md).ConstructorOptions

## Table of contents

### Properties

- [clientId](flexportapi.ConstructorOptions.md#clientid)
- [clientSecret](flexportapi.ConstructorOptions.md#clientsecret)
- [domain](flexportapi.ConstructorOptions.md#domain)
- [enableLogging](flexportapi.ConstructorOptions.md#enablelogging)

## Properties

### clientId

• **clientId**: `string`

___

### clientSecret

• **clientSecret**: `string`

___

### domain

• **domain**: `string`

___

### enableLogging

• `Optional` **enableLogging**: `boolean`
