[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Metadata

# Interface: Metadata

[Interfaces/Objects](../modules/Interfaces_Objects.md).Metadata

## Table of contents

### Properties

- [size](Interfaces_Objects.Metadata.md#size)

## Properties

### size

• `Optional` **size**: `string`[]
