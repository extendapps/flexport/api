[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / BookingLineItem

# Interface: BookingLineItem

[Interfaces/Objects](../modules/Interfaces_Objects.md).BookingLineItem

## Table of contents

### Properties

- [\_object](Interfaces_Objects.BookingLineItem.md#_object)
- [booking](Interfaces_Objects.BookingLineItem.md#booking)
- [id](Interfaces_Objects.BookingLineItem.md#id)
- [purchase\_order\_line\_item](Interfaces_Objects.BookingLineItem.md#purchase_order_line_item)
- [units](Interfaces_Objects.BookingLineItem.md#units)

## Properties

### \_object

• `Optional` **\_object**: ``"/purchase_orders/booking_line_item"``

___

### booking

• **booking**: [`ObjectRef`](Interfaces_Objects.ObjectRef.md) \| [`Booking`](Interfaces_Objects.Booking.md)

___

### id

• **id**: `number`

___

### purchase\_order\_line\_item

• **purchase\_order\_line\_item**: [`ObjectRef`](Interfaces_Objects.ObjectRef.md) \| [`PurchaseOrderLineItem`](Interfaces_Objects.PurchaseOrderLineItem.md)

___

### units

• **units**: `number`
