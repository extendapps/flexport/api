[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / AirPortDetail

# Interface: AirPortDetail

[Interfaces/Objects](../modules/Interfaces_Objects.md).AirPortDetail

## Hierarchy

- [`Detail`](Interfaces_Objects.Detail.md)

  ↳ **`AirPortDetail`**

## Table of contents

### Properties

- [\_object](Interfaces_Objects.AirPortDetail.md#_object)
- [iata\_code](Interfaces_Objects.AirPortDetail.md#iata_code)
- [icao\_code](Interfaces_Objects.AirPortDetail.md#icao_code)
- [port\_code](Interfaces_Objects.AirPortDetail.md#port_code)

## Properties

### \_object

• **\_object**: ``"/air/port"``

#### Overrides

[Detail](Interfaces_Objects.Detail.md).[_object](Interfaces_Objects.Detail.md#_object)

___

### iata\_code

• `Optional` **iata\_code**: `string`

___

### icao\_code

• `Optional` **icao\_code**: `string`

___

### port\_code

• `Optional` **port\_code**: `string`

#### Inherited from

[Detail](Interfaces_Objects.Detail.md).[port_code](Interfaces_Objects.Detail.md#port_code)
