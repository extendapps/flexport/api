[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Address

# Interface: Address

[Interfaces/Objects](../modules/Interfaces_Objects.md).Address

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Address.md#_object)
- [city](Interfaces_Objects.Address.md#city)
- [country](Interfaces_Objects.Address.md#country)
- [country\_code](Interfaces_Objects.Address.md#country_code)
- [ref](Interfaces_Objects.Address.md#ref)
- [state](Interfaces_Objects.Address.md#state)
- [street\_address](Interfaces_Objects.Address.md#street_address)
- [street\_address2](Interfaces_Objects.Address.md#street_address2)
- [timezone](Interfaces_Objects.Address.md#timezone)
- [unlocode](Interfaces_Objects.Address.md#unlocode)
- [zip](Interfaces_Objects.Address.md#zip)

## Properties

### \_object

• **\_object**: ``"/address"``

___

### city

• **city**: `string`

___

### country

• **country**: `string`

___

### country\_code

• **country\_code**: `string`

___

### ref

• **ref**: `string`

___

### state

• **state**: `string`

___

### street\_address

• **street\_address**: `string`

___

### street\_address2

• `Optional` **street\_address2**: `string`

___

### timezone

• **timezone**: `string`

___

### unlocode

• `Optional` **unlocode**: `string`

___

### zip

• **zip**: `string`
