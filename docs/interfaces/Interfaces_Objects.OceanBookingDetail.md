[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / OceanBookingDetail

# Interface: OceanBookingDetail

[Interfaces/Objects](../modules/Interfaces_Objects.md).OceanBookingDetail

## Table of contents

### Properties

- [\_object](Interfaces_Objects.OceanBookingDetail.md#_object)

## Properties

### \_object

• **\_object**: ``"/ocean/booking"``
