[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / PurchaseOrderProduct

# Interface: PurchaseOrderProduct

[Interfaces/Objects](../modules/Interfaces_Objects.md).PurchaseOrderProduct

## Table of contents

### Properties

- [\_object](Interfaces_Objects.PurchaseOrderProduct.md#_object)
- [color](Interfaces_Objects.PurchaseOrderProduct.md#color)
- [country\_of\_origin](Interfaces_Objects.PurchaseOrderProduct.md#country_of_origin)
- [dangerous](Interfaces_Objects.PurchaseOrderProduct.md#dangerous)
- [ean\_ucc\_13](Interfaces_Objects.PurchaseOrderProduct.md#ean_ucc_13)
- [ean\_ucc\_8](Interfaces_Objects.PurchaseOrderProduct.md#ean_ucc_8)
- [lot\_number](Interfaces_Objects.PurchaseOrderProduct.md#lot_number)
- [name](Interfaces_Objects.PurchaseOrderProduct.md#name)
- [product\_category](Interfaces_Objects.PurchaseOrderProduct.md#product_category)
- [size](Interfaces_Objects.PurchaseOrderProduct.md#size)
- [sku](Interfaces_Objects.PurchaseOrderProduct.md#sku)
- [style](Interfaces_Objects.PurchaseOrderProduct.md#style)
- [upc](Interfaces_Objects.PurchaseOrderProduct.md#upc)

## Properties

### \_object

• `Optional` **\_object**: ``"/purchase_orders/line_item_product"``

___

### color

• `Optional` **color**: `string`

___

### country\_of\_origin

• `Optional` **country\_of\_origin**: `string`

___

### dangerous

• `Optional` **dangerous**: `boolean`

___

### ean\_ucc\_13

• `Optional` **ean\_ucc\_13**: `string`

___

### ean\_ucc\_8

• `Optional` **ean\_ucc\_8**: `string`

___

### lot\_number

• `Optional` **lot\_number**: `string`

___

### name

• **name**: `string`

___

### product\_category

• `Optional` **product\_category**: `string`

___

### size

• `Optional` **size**: `string`

___

### sku

• **sku**: `string`

___

### style

• `Optional` **style**: `string`

___

### upc

• `Optional` **upc**: `string`
