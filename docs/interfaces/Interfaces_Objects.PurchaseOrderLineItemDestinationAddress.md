[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / PurchaseOrderLineItemDestinationAddress

# Interface: PurchaseOrderLineItemDestinationAddress

[Interfaces/Objects](../modules/Interfaces_Objects.md).PurchaseOrderLineItemDestinationAddress

## Table of contents

### Properties

- [\_object](Interfaces_Objects.PurchaseOrderLineItemDestinationAddress.md#_object)
- [address](Interfaces_Objects.PurchaseOrderLineItemDestinationAddress.md#address)
- [units](Interfaces_Objects.PurchaseOrderLineItemDestinationAddress.md#units)

## Properties

### \_object

• `Optional` **\_object**: ``"/purchase_orders/line_items/destination_address"``

___

### address

• **address**: [`Address`](Interfaces_Objects.Address.md)

___

### units

• **units**: `number`
