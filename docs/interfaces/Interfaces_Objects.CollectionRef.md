[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / CollectionRef

# Interface: CollectionRef<ObjectType\>

[Interfaces/Objects](../modules/Interfaces_Objects.md).CollectionRef

## Type parameters

| Name |
| :------ |
| `ObjectType` |

## Table of contents

### Properties

- [\_object](Interfaces_Objects.CollectionRef.md#_object)
- [data](Interfaces_Objects.CollectionRef.md#data)
- [link](Interfaces_Objects.CollectionRef.md#link)
- [next](Interfaces_Objects.CollectionRef.md#next)
- [prev](Interfaces_Objects.CollectionRef.md#prev)
- [ref\_type](Interfaces_Objects.CollectionRef.md#ref_type)
- [total\_count](Interfaces_Objects.CollectionRef.md#total_count)

## Properties

### \_object

• **\_object**: ``"/api/refs/collection"`` \| ``"/api/collections/paginated"``

___

### data

• **data**: `ObjectType`[]

___

### link

• **link**: `string`

___

### next

• `Optional` **next**: `string`

___

### prev

• `Optional` **prev**: `string`

___

### ref\_type

• **ref\_type**: `string`

___

### total\_count

• **total\_count**: `number`
