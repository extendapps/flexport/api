[Flexport API NetSuite module](../README.md) / [Interfaces/Location](../modules/Interfaces_Location.md) / GetLocationsOptions

# Interface: GetLocationsOptions

[Interfaces/Location](../modules/Interfaces_Location.md).GetLocationsOptions

## Hierarchy

- [`LocationExpandable`](Interfaces_Location.LocationExpandable.md)

  ↳ **`GetLocationsOptions`**

## Table of contents

### Methods

- [BadRequest](Interfaces_Location.GetLocationsOptions.md#badrequest)
- [Failed](Interfaces_Location.GetLocationsOptions.md#failed)
- [Unauthorized](Interfaces_Location.GetLocationsOptions.md#unauthorized)

### Properties

- [OK](Interfaces_Location.GetLocationsOptions.md#ok)
- [expand](Interfaces_Location.GetLocationsOptions.md#expand)
- [filters](Interfaces_Location.GetLocationsOptions.md#filters)

## Methods

### BadRequest

▸ `Optional` **BadRequest**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[LocationExpandable](Interfaces_Location.LocationExpandable.md).[BadRequest](Interfaces_Location.LocationExpandable.md#badrequest)

___

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

[LocationExpandable](Interfaces_Location.LocationExpandable.md).[Failed](Interfaces_Location.LocationExpandable.md#failed)

___

### Unauthorized

▸ `Optional` **Unauthorized**(`error`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`FlexportError`](Interfaces_BaseObject.FlexportError.md) |

#### Returns

`void`

#### Inherited from

[LocationExpandable](Interfaces_Location.LocationExpandable.md).[Unauthorized](Interfaces_Location.LocationExpandable.md#unauthorized)

## Properties

### OK

• **OK**: (`flexportResponse`: [`FlexportCollectionResponse`](Interfaces_BaseObject.FlexportCollectionResponse.md)<[`Location`](Interfaces_Objects.Location.md)\>) => `void`

#### Type declaration

▸ (`flexportResponse`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `flexportResponse` | [`FlexportCollectionResponse`](Interfaces_BaseObject.FlexportCollectionResponse.md)<[`Location`](Interfaces_Objects.Location.md)\> |

##### Returns

`void`

___

### expand

• `Optional` **expand**: ``"company"``

Include Company in the `company` property

#### Inherited from

[LocationExpandable](Interfaces_Location.LocationExpandable.md).[expand](Interfaces_Location.LocationExpandable.md#expand)

___

### filters

• `Optional` **filters**: `Object`

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `f.ref?` | `string` | The ref for the Location |
| `page?` | `number` | Page number of the page to retrieve |
| `per?` | `number` | Count of items in each page Should be between 1 and 100 (inclusive) |
