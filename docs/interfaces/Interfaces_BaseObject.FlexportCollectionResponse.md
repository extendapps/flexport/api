[Flexport API NetSuite module](../README.md) / [Interfaces/BaseObject](../modules/Interfaces_BaseObject.md) / FlexportCollectionResponse

# Interface: FlexportCollectionResponse<ObjectType\>

[Interfaces/BaseObject](../modules/Interfaces_BaseObject.md).FlexportCollectionResponse

## Type parameters

| Name |
| :------ |
| `ObjectType` |

## Table of contents

### Properties

- [\_object](Interfaces_BaseObject.FlexportCollectionResponse.md#_object)
- [data](Interfaces_BaseObject.FlexportCollectionResponse.md#data)
- [errors](Interfaces_BaseObject.FlexportCollectionResponse.md#errors)
- [self](Interfaces_BaseObject.FlexportCollectionResponse.md#self)
- [version](Interfaces_BaseObject.FlexportCollectionResponse.md#version)

## Properties

### \_object

• **\_object**: ``"/api/response"``

String representing the object’s type. Always /api/response for this object.

___

### data

• **data**: [`Pagination`](Interfaces_BaseObject.Pagination.md)<`ObjectType`\>

The resource data requested for a successful response. null on error.
In the case of multiple results, this will contain the Pagination data

___

### errors

• `Optional` **errors**: [`FlexportError`](Interfaces_BaseObject.FlexportError.md)[]

The error object indicating what went wrong

___

### self

• **self**: `string`

The full path to the resource(s) requested

___

### version

• **version**: `number`

The version of the request
