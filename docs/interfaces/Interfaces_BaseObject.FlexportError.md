[Flexport API NetSuite module](../README.md) / [Interfaces/BaseObject](../modules/Interfaces_BaseObject.md) / FlexportError

# Interface: FlexportError

[Interfaces/BaseObject](../modules/Interfaces_BaseObject.md).FlexportError

## Table of contents

### Properties

- [\_object](Interfaces_BaseObject.FlexportError.md#_object)
- [code](Interfaces_BaseObject.FlexportError.md#code)
- [message](Interfaces_BaseObject.FlexportError.md#message)
- [status](Interfaces_BaseObject.FlexportError.md#status)

## Properties

### \_object

• **\_object**: ``"/api/error"``

String representing the object’s type. Always /api/error for this object.

___

### code

• **code**: `string`

Specialized identifier for this type of error in our API

___

### message

• **message**: `string`

Human readable error message

___

### status

• **status**: `number`

The HTTP status code (client or server error) made available for consumption from the body of the response
