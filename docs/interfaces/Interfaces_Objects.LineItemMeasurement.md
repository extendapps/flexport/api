[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / LineItemMeasurement

# Interface: LineItemMeasurement

[Interfaces/Objects](../modules/Interfaces_Objects.md).LineItemMeasurement

## Table of contents

### Properties

- [\_object](Interfaces_Objects.LineItemMeasurement.md#_object)
- [measure\_type](Interfaces_Objects.LineItemMeasurement.md#measure_type)
- [unit\_of\_measure](Interfaces_Objects.LineItemMeasurement.md#unit_of_measure)
- [value](Interfaces_Objects.LineItemMeasurement.md#value)

## Properties

### \_object

• `Optional` **\_object**: ``"/line_item_measurement"``

___

### measure\_type

• **measure\_type**: [`MeasureType`](../modules/Interfaces_Objects.md#measuretype)

___

### unit\_of\_measure

• **unit\_of\_measure**: `string`

___

### value

• **value**: `string`
