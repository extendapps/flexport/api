[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / VatNumber

# Interface: VatNumber

[Interfaces/Objects](../modules/Interfaces_Objects.md).VatNumber

## Table of contents

### Properties

- [\_object](Interfaces_Objects.VatNumber.md#_object)
- [country\_code](Interfaces_Objects.VatNumber.md#country_code)
- [number](Interfaces_Objects.VatNumber.md#number)

## Properties

### \_object

• **\_object**: ``"/company_entity/vat_number"``

___

### country\_code

• **country\_code**: `string`

___

### number

• **number**: `string`
