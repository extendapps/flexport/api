[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Location

# Interface: Location

[Interfaces/Objects](../modules/Interfaces_Objects.md).Location

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Location.md#_object)
- [address](Interfaces_Objects.Location.md#address)
- [company](Interfaces_Objects.Location.md#company)
- [contacts](Interfaces_Objects.Location.md#contacts)
- [editable](Interfaces_Objects.Location.md#editable)
- [id](Interfaces_Objects.Location.md#id)
- [metadata](Interfaces_Objects.Location.md#metadata)
- [name](Interfaces_Objects.Location.md#name)
- [ref](Interfaces_Objects.Location.md#ref)

## Properties

### \_object

• **\_object**: ``"/network/location"``

___

### address

• **address**: [`Address`](Interfaces_Objects.Address.md)

___

### company

• **company**: [`Company`](Interfaces_Objects.Company.md) \| [`ObjectRef`](Interfaces_Objects.ObjectRef.md)

___

### contacts

• **contacts**: [`CollectionRef`](Interfaces_Objects.CollectionRef.md)<[`Contact`](Interfaces_Objects.Contact.md)\>

___

### editable

• **editable**: `boolean`

___

### id

• **id**: `string`

___

### metadata

• **metadata**: [`Metadata`](Interfaces_Objects.Metadata.md)

___

### name

• **name**: `string`

___

### ref

• **ref**: `string`
