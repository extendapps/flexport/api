[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / Company

# Interface: Company

[Interfaces/Objects](../modules/Interfaces_Objects.md).Company

## Table of contents

### Properties

- [\_object](Interfaces_Objects.Company.md#_object)
- [contacts](Interfaces_Objects.Company.md#contacts)
- [editable](Interfaces_Objects.Company.md#editable)
- [entities](Interfaces_Objects.Company.md#entities)
- [id](Interfaces_Objects.Company.md#id)
- [locations](Interfaces_Objects.Company.md#locations)
- [metadata](Interfaces_Objects.Company.md#metadata)
- [name](Interfaces_Objects.Company.md#name)
- [ref](Interfaces_Objects.Company.md#ref)

## Properties

### \_object

• **\_object**: ``"/network/company"``

___

### contacts

• **contacts**: [`CollectionRef`](Interfaces_Objects.CollectionRef.md)<[`Contact`](Interfaces_Objects.Contact.md)\>

___

### editable

• **editable**: `boolean`

___

### entities

• **entities**: [`CompanyEntity`](Interfaces_Objects.CompanyEntity.md)[]

___

### id

• **id**: `string`

___

### locations

• **locations**: [`CollectionRef`](Interfaces_Objects.CollectionRef.md)<[`Location`](Interfaces_Objects.Location.md)\>

___

### metadata

• **metadata**: [`Metadata`](Interfaces_Objects.Metadata.md)

___

### name

• **name**: `string`

___

### ref

• **ref**: `string`
