[Flexport API NetSuite module](../README.md) / [Interfaces/Objects](../modules/Interfaces_Objects.md) / HsCode

# Interface: HsCode

[Interfaces/Objects](../modules/Interfaces_Objects.md).HsCode

## Table of contents

### Properties

- [\_object](Interfaces_Objects.HsCode.md#_object)
- [code](Interfaces_Objects.HsCode.md#code)
- [country\_code](Interfaces_Objects.HsCode.md#country_code)
- [description](Interfaces_Objects.HsCode.md#description)

## Properties

### \_object

• `Optional` **\_object**: ``"/hs_code"``

___

### code

• **code**: `string`

___

### country\_code

• **country\_code**: `string`

___

### description

• `Optional` **description**: `string`
