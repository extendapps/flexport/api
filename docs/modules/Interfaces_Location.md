[Flexport API NetSuite module](../README.md) / Interfaces/Location

# Module: Interfaces/Location

## Table of contents

### Method Interfaces

- [GetLocationsOptions](../interfaces/Interfaces_Location.GetLocationsOptions.md)
- [LocationExpandable](../interfaces/Interfaces_Location.LocationExpandable.md)
