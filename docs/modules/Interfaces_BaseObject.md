[Flexport API NetSuite module](../README.md) / Interfaces/BaseObject

# Module: Interfaces/BaseObject

## Table of contents

### Interfaces

- [FlexportCollectionResponse](../interfaces/Interfaces_BaseObject.FlexportCollectionResponse.md)
- [FlexportError](../interfaces/Interfaces_BaseObject.FlexportError.md)
- [FlexportSingleResponse](../interfaces/Interfaces_BaseObject.FlexportSingleResponse.md)
- [Pagination](../interfaces/Interfaces_BaseObject.Pagination.md)
