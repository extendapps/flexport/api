[Flexport API NetSuite module](../README.md) / Interfaces/Objects

# Module: Interfaces/Objects

## Table of contents

### Interfaces

- [Address](../interfaces/Interfaces_Objects.Address.md)
- [AirBookingDetail](../interfaces/Interfaces_Objects.AirBookingDetail.md)
- [AirPortDetail](../interfaces/Interfaces_Objects.AirPortDetail.md)
- [Booking](../interfaces/Interfaces_Objects.Booking.md)
- [BookingLineItem](../interfaces/Interfaces_Objects.BookingLineItem.md)
- [Cargo](../interfaces/Interfaces_Objects.Cargo.md)
- [CollectionRef](../interfaces/Interfaces_Objects.CollectionRef.md)
- [Company](../interfaces/Interfaces_Objects.Company.md)
- [CompanyEntity](../interfaces/Interfaces_Objects.CompanyEntity.md)
- [Contact](../interfaces/Interfaces_Objects.Contact.md)
- [Container](../interfaces/Interfaces_Objects.Container.md)
- [Detail](../interfaces/Interfaces_Objects.Detail.md)
- [FlexportEvent](../interfaces/Interfaces_Objects.FlexportEvent.md)
- [HsCode](../interfaces/Interfaces_Objects.HsCode.md)
- [LineItemMeasurement](../interfaces/Interfaces_Objects.LineItemMeasurement.md)
- [Location](../interfaces/Interfaces_Objects.Location.md)
- [Metadata](../interfaces/Interfaces_Objects.Metadata.md)
- [Money](../interfaces/Interfaces_Objects.Money.md)
- [ObjectRef](../interfaces/Interfaces_Objects.ObjectRef.md)
- [OceanBookingDetail](../interfaces/Interfaces_Objects.OceanBookingDetail.md)
- [Party](../interfaces/Interfaces_Objects.Party.md)
- [Place](../interfaces/Interfaces_Objects.Place.md)
- [PurchaseOrder](../interfaces/Interfaces_Objects.PurchaseOrder.md)
- [PurchaseOrderLineItem](../interfaces/Interfaces_Objects.PurchaseOrderLineItem.md)
- [PurchaseOrderLineItemDestinationAddress](../interfaces/Interfaces_Objects.PurchaseOrderLineItemDestinationAddress.md)
- [PurchaseOrderProduct](../interfaces/Interfaces_Objects.PurchaseOrderProduct.md)
- [RailPortDetail](../interfaces/Interfaces_Objects.RailPortDetail.md)
- [RoadPortDetail](../interfaces/Interfaces_Objects.RoadPortDetail.md)
- [SeaPortDetail](../interfaces/Interfaces_Objects.SeaPortDetail.md)
- [Shipment](../interfaces/Interfaces_Objects.Shipment.md)
- [ShipmentCreatedEvent](../interfaces/Interfaces_Objects.ShipmentCreatedEvent.md)
- [ShipmentNode](../interfaces/Interfaces_Objects.ShipmentNode.md)
- [Terminal](../interfaces/Interfaces_Objects.Terminal.md)
- [TruckingBookingDetail](../interfaces/Interfaces_Objects.TruckingBookingDetail.md)
- [VatNumber](../interfaces/Interfaces_Objects.VatNumber.md)

### Type Aliases

- [BookingStatus](Interfaces_Objects.md#bookingstatus)
- [FreightPaymentTerms](Interfaces_Objects.md#freightpaymentterms)
- [Incoterm](Interfaces_Objects.md#incoterm)
- [LineType](Interfaces_Objects.md#linetype)
- [MeasureType](Interfaces_Objects.md#measuretype)
- [OrderClass](Interfaces_Objects.md#orderclass)
- [Priority](Interfaces_Objects.md#priority)
- [Role](Interfaces_Objects.md#role)
- [ShipmentLeg](Interfaces_Objects.md#shipmentleg)
- [Status](Interfaces_Objects.md#status)
- [Tag](Interfaces_Objects.md#tag)
- [TransportMode](Interfaces_Objects.md#transportmode)
- [UnitOfMeasure](Interfaces_Objects.md#unitofmeasure)

## Type Aliases

### BookingStatus

Ƭ **BookingStatus**: ``"draft"`` \| ``"archived"`` \| ``"submitted"`` \| ``"booked"`` \| ``"shipment"``

___

### FreightPaymentTerms

Ƭ **FreightPaymentTerms**: ``"freight_collect"`` \| ``"freight_prepaid"``

___

### Incoterm

Ƭ **Incoterm**: ``"EXW"`` \| ``"FCA"`` \| ``"FAS"`` \| ``"FOB"`` \| ``"CPT"`` \| ``"CFR"`` \| ``"CIF"`` \| ``"CIP"`` \| ``"DAT"`` \| ``"DAP"`` \| ``"DDP"`` \| ``"DPU"``

___

### LineType

Ƭ **LineType**: ``"main_line"`` \| ``"sub_line"`` \| ``"component_line"``

___

### MeasureType

Ƭ **MeasureType**: ``"length"`` \| ``"width"`` \| ``"height"`` \| ``"gross_weight"`` \| ``"net_weight"`` \| ``"net_net_weight"`` \| ``"gross_volume"`` \| ``"volume_weight"``

___

### OrderClass

Ƭ **OrderClass**: ``"purchase_order"`` \| ``"sales_order"`` \| ``"transfer_order"`` \| ``"delivery_order"`` \| ``"work_order"``

___

### Priority

Ƭ **Priority**: ``"standard"`` \| ``"high"``

___

### Role

Ƭ **Role**: ``"buyer"`` \| ``"seller"`` \| ``"owner"`` \| ``"shipper"`` \| ``"consignee"`` \| ``"freight_forwarder"`` \| ``"notify_party"`` \| ``"customs_broker"`` \| ``"carrier"`` \| ``"manufacturer"`` \| ``"buyers_agent"`` \| ``"sellers_agent"``

___

### ShipmentLeg

Ƭ **ShipmentLeg**: [`Company`](../interfaces/Interfaces_Objects.Company.md)

___

### Status

Ƭ **Status**: ``"open"`` \| ``"closed"`` \| ``"cancelled"``

___

### Tag

Ƭ **Tag**: ``"origin_address"`` \| ``"destination_address"`` \| ``"port_of_loading"`` \| ``"port_of_unloading"`` \| ``"port_of_calling"`` \| ``"fmc_port_of_unloading"`` \| ``"customs_entry"`` \| ``"transshipment"`` \| ``"place_of_delivery"``

___

### TransportMode

Ƭ **TransportMode**: ``"ocean"`` \| ``"air"`` \| ``"truck"``

___

### UnitOfMeasure

Ƭ **UnitOfMeasure**: ``"BBL"`` \| ``"CAR"`` \| ``"CGM"`` \| ``"CKG"`` \| ``"CM"`` \| ``"CM2"`` \| ``"CTN"`` \| ``"CYK"`` \| ``"DOZ"`` \| ``"DPC"`` \| ``"DPR"`` \| ``"DS"`` \| ``"FBM"`` \| ``"G"`` \| ``"GBQ"`` \| ``"GR"`` \| ``"GRL"`` \| ``"HUN"`` \| ``"IRG"`` \| ``"JWL"`` \| ``"K"`` \| ``"KG"`` \| ``"KM3"`` \| ``"KM"`` \| ``"KWH"`` \| ``"L"`` \| ``"LNM"`` \| ``"M"`` \| ``"M2"`` \| ``"M3"`` \| ``"MBQ"`` \| ``"NO"`` \| ``"OSG"`` \| ``"PCS"`` \| ``"PDG"`` \| ``"PFL"`` \| ``"PK"`` \| ``"PRS"`` \| ``"PTG"`` \| ``"PX"`` \| ``"RHG"`` \| ``"RUG"`` \| ``"T"`` \| ``"TDWB"`` \| ``"THS"`` \| ``"W"`` \| ``"X"`` \| ``"20"`` \| ``"21"`` \| ``"2W"`` \| ``"43"`` \| ``"AS"`` \| ``"BA"`` \| ``"BC"`` \| ``"BD"`` \| ``"BG"`` \| ``"BJ"`` \| ``"BK"`` \| ``"BN"`` \| ``"BO"`` \| ``"BU"`` \| ``"BX"`` \| ``"CA"`` \| ``"CB"`` \| ``"CC"`` \| ``"CF"`` \| ``"CI"`` \| ``"CN"`` \| ``"CP"`` \| ``"CQ"`` \| ``"CR"`` \| ``"CS"`` \| ``"CT"`` \| ``"CX"`` \| ``"CY"`` \| ``"DF"`` \| ``"DH"`` \| ``"DR"`` \| ``"DZ"`` \| ``"EA"`` \| ``"FT"`` \| ``"GA"`` \| ``"GRAM"`` \| ``"GS"`` \| ``"IN"`` \| ``"JO"`` \| ``"JR"`` \| ``"LB"`` \| ``"LT"`` \| ``"MP"`` \| ``"MR"`` \| ``"MT"`` \| ``"OZ"`` \| ``"PC"`` \| ``"PQ"`` \| ``"PR"`` \| ``"PS"`` \| ``"PT"`` \| ``"QT"`` \| ``"RA"`` \| ``"RL"`` \| ``"RM"`` \| ``"RO"`` \| ``"SC"`` \| ``"SF"`` \| ``"SH"`` \| ``"SI"`` \| ``"SJ"`` \| ``"SL"`` \| ``"SM"`` \| ``"SO"`` \| ``"SQ"`` \| ``"ST"`` \| ``"SV"`` \| ``"SX"`` \| ``"SY"`` \| ``"TE"`` \| ``"TN"`` \| ``"TO"`` \| ``"UN"`` \| ``"YD"`` \| ``"Z3"``

**`Copyright`**

2021 ExtendApps, Inc.

**`Author`**

Darren Hill darren@extendapps.com
