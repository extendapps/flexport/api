[Flexport API NetSuite module](../README.md) / Interfaces/Company

# Module: Interfaces/Company

## Table of contents

### Method Interfaces

- [CompanyExpandable](../interfaces/Interfaces_Company.CompanyExpandable.md)
- [GetCompaniesOptions](../interfaces/Interfaces_Company.GetCompaniesOptions.md)
- [GetCompanyOptions](../interfaces/Interfaces_Company.GetCompanyOptions.md)
- [GetMyCompanyOptions](../interfaces/Interfaces_Company.GetMyCompanyOptions.md)
