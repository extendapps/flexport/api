[Flexport API NetSuite module](../README.md) / Interfaces/BaseOptions

# Module: Interfaces/BaseOptions

## Table of contents

### Interfaces

- [BaseExpandableOptions](../interfaces/Interfaces_BaseOptions.BaseExpandableOptions.md)
- [BaseOptions](../interfaces/Interfaces_BaseOptions.BaseOptions.md)
