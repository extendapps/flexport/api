[Flexport API NetSuite module](../README.md) / Interfaces/PurchaseOrder

# Module: Interfaces/PurchaseOrder

## Table of contents

### Method Interfaces

- [GetPurchaseOrderOptions](../interfaces/Interfaces_PurchaseOrder.GetPurchaseOrderOptions.md)
- [GetPurchaseOrdersOptions](../interfaces/Interfaces_PurchaseOrder.GetPurchaseOrdersOptions.md)
- [PurchaseOrderExpandable](../interfaces/Interfaces_PurchaseOrder.PurchaseOrderExpandable.md)
- [UpsertPurchaseOrdersOptions](../interfaces/Interfaces_PurchaseOrder.UpsertPurchaseOrdersOptions.md)
